#include "resloader.h"

#include <QDebug>
#include <iostream>


int main(int argc, char *argv[])
{
    double data[72][19];

    if (argc == 3) {
        QString fileName = argv[1];
        QString phi0Arg = argv[2];
        double phi0 = phi0Arg.toDouble();
        // qDebug() << "file name =" << fileName;
        // qDebug() << "phi0 =" << phi0;
        res::readData(fileName, data);
        res::fillPhiZero(phi0, data);
        for (int j = 0; j < 19; ++j) {
            for (int i = 0; i < 71; ++i) {
                std::cout << data[i][j] << " ";
            }
            std::cout << data[71][j];
            std::cout << std::endl;
        }
        return 0;
    }
    else {
        qDebug() << "Invalid number of command line arguments!";
        qDebug() << "./resconv [RES file] [phi0]";
        return 1;
    }
}
