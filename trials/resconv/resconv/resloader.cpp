#include "resloader.h"

#include <QDebug>

#include <QFile>
#include <QRegExp>
#include <QStringList>
#include <QTextStream>

#include <exception>

void res::readData(const QString &path, double targets[72][19])
{
    QVector<QString> lines = readLines(path);
    int firstIndex = findFirstLine(lines);
    fillData(lines, firstIndex, targets);
}

QVector<QString> res::readLines(const QString &path)
{
    QVector<QString> lines;
    QFile resFile(path);
    if (resFile.open(QIODevice::ReadOnly)) {
        QTextStream stream(&resFile);
        while (stream.atEnd() == false) {
            QString line = stream.readLine();
            lines.push_back(line);
        }
    }
    else {
        throw std::invalid_argument("Unable to open the RES file!");
    }
    return lines;
}

int res::findFirstLine(const QVector<QString> &lines)
{
    int n = 0;
    int index = 0;
    int i = 0;

    for (const QString& line : lines) {
        if (line.startsWith(" 200")) {
            ++n;
            index = i;
        }
        ++i;
    }
    if (n != 3) {
        throw std::runtime_error("Invalid RES file!");
    }
    // NOTE: Line after the " 200"
    ++index;
    return index;
}

QVector<double> res::readRowValues(const QVector<QString> &lines, int index)
{
    QString row;
    for (int i = index; i < index + 8; ++i) {
        row += lines[i];
    }
    QStringList items = row.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    QVector<double> values;
    for (const QString& item : items) {
        values.push_back(item.toDouble());
    }
    if (values.size() != 72) {
        throw std::runtime_error("Invalid number of values in a row!");
    }
    return values;
}

void res::fillData(const QVector<QString> &lines, int index, double targets[72][19])
{
    int j = 1;
    for (int i = index; i < index + (18 * 8); i += 8) {
        QVector<double> values = readRowValues(lines, i);
        for (int k = 0; k < 72; ++k) {
            targets[k][j] = values[k];
        }
        ++j;
    }
}

void res::fillPhiZero(double phi0, double targets[72][19])
{
    for (int i = 0; i < 72; ++i) {
        targets[i][0] = phi0;
    }
}
