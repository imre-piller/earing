import csv
import math
from matplotlib import pyplot as plt


def read_params(path):
    with open(path) as param_file:
        field_names = ['index',
                       'alpha_1', 'mu_1', 'sigma_1',
                       'alpha_2', 'mu_2', 'sigma_2',
                       'alpha_3', 'mu_3', 'sigma_3',
                       'alpha_4', 'mu_4', 'sigma_4'
                      ]
        reader = csv.DictReader(param_file,
                                fieldnames=field_names,
                               delimiter=' ')
        params = []
        for row in reader:
            row.pop(None, None)
            item = {k: float(v) for k, v in row.items()}
            params.append(item)
        return params


def calc_rose_values(params):
    values = []
    for row in params:
        value = 0.0
        for i in ['1', '2', '3', '4']:
            value += row['mu_{}'.format(i)] * row['alpha_{}'.format(i)]
        values.append(value)
    return values


def save_rose_values(values):
    with open('results/rose.csv', 'w') as rose_file:
        for value in values:
            rose_file.write('{}\n'.format(value))


path = 'results/params.csv'
params = read_params(path)
phis = [5 * i for i in range(72)]
values = calc_rose_values(params)
save_rose_values(values)
plt.figure(figsize=(12, 10))
ax = plt.axes()
ax.grid()
plt.plot(phis, values)
plt.savefig('results/rose.png')
plt.close()

