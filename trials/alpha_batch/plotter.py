import csv
import math
import os
import sys
import matplotlib.pyplot as plt
import numpy as np


def load_samples(path):
    """
    Load the sample of measurements.
    """
    samples = [[] for _ in range(72)]
    with open(path) as sample_file:
        csv_reader = csv.reader(sample_file, delimiter=' ')
        for row in csv_reader:
            for j in range(72):
                samples[j].append(float(row[j]))
    return samples


def load_params(path):
    """
    Load the parameters of the fitted gaussian curves.
    It results (alpha, mu, sigma) parameters for times.
    """
    params = []
    with open(path) as param_file:
        csv_reader = csv.reader(param_file, delimiter=' ')
        for row in csv_reader:
            values = []
            for i in range(1, 13):
                values.append(float(row[i]))
            params.append(values)
    return params


def gaussian(x, alpha, mu, sigma):
    return (alpha / (math.sqrt(2 * math.pi) * sigma)) * math.exp(-((x - mu)*(x - mu))/(2 * sigma * sigma))


def gaussian_on_range(alpha, mu, sigma):
    values = []
    for x in range(0, 91):
        values.append(gaussian(x, alpha, mu, sigma))
    return values


def calc_approx(curve_params):
    result = [
        gaussian_on_range(curve_params[0], curve_params[1], curve_params[2]),
        gaussian_on_range(curve_params[3], curve_params[4], curve_params[5]),
        gaussian_on_range(curve_params[6], curve_params[7], curve_params[8]),
        gaussian_on_range(curve_params[9], curve_params[10], curve_params[11])
    ]
    return result


def calc_cumulated(values):
    cumulated = []
    for x in range(91):
        cumulated.append(values[0][x] + values[1][x] + values[2][x] + values[3][x])
    return cumulated


def calc_square_error(calculated, target):
    square_error = 0
    for i in range(len(target)):
        square_error += (calculated[i * 5] - target[i])**2
    return square_error


def calc_max_error(calculated, target):
    errors = []
    for i in range(len(target)):
        errors.append(calculated[i * 5] - target[i])
    return max(errors)


def plot_fitted_curves(path):
    samples = load_samples(path)
    params = load_params('results/params.csv')
    dir_name = 'results/fitted_curves'
    os.mkdir(dir_name)
    
    for n in range(72):
        gauss_curve_values = calc_approx(params[n])
        cumulated = calc_cumulated(gauss_curve_values)
        square_error = calc_square_error(cumulated, samples[n])
        
        plt.figure(figsize=(12, 10))

        plt.xlabel('chi')
        plt.ylabel('value')
        plt.title('phi = {}, error = {}'.format(n * 5, square_error))
        axes = plt.gca()
        axes.set_xlim([0, 90])
        axes.set_ylim([-0.2, 5])
        plt.grid(True)

        plt.plot([i for i in range(0, 95, 5)], samples[n], 'b')

        for value in gauss_curve_values:
            plt.plot([i for i in range(91)], value, 'g')

        plt.plot([i for i in range(91)], cumulated, 'r')

        plt.savefig(dir_name + "/phi_{:03d}.png".format(n * 5))
        plt.close()


def plot_errors(path):
    samples = load_samples(path)
    params = load_params('results/params.csv')
    mse_error_values = []
    max_error_values = []
    phi_values = []
    for n in range(72):
        gauss_curve_values = calc_approx(params[n])
        cumulated = calc_cumulated(gauss_curve_values)
        sse_error = calc_square_error(cumulated, samples[n])
        max_error = calc_max_error(cumulated, samples[n])
        mse_error_values.append(sse_error / 19)
        max_error_values.append(max_error)
        phi_values.append(n * 5.0)

    ax = plt.subplot()
    ax.plot(phi_values, mse_error_values, color='b', linewidth=3)
    ax.plot(phi_values, max_error_values, color='r', linewidth=3)
    ax.grid(True)
    plt.savefig('results/errors.png')
    plt.close()


if __name__ == '__main__':
    path = sys.argv[1]
    plot_fitted_curves(path)
    plot_errors(path)

