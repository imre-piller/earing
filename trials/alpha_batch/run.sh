#!/bin/bash

for fn in $(ls ../../samples);
do
    echo '====' $fn
    rm data.txt 2> /dev/null
    cp ../../samples/$fn data.txt
    rm -rf results 2> /dev/null
    mkdir results

    echo 'Search parameters ...'
    ./genetic.exe
    mv params.csv results/params.csv

    echo 'Draw plots ...'
    python plotter.py data.txt
    python rose.py

    mv results $fn
done
