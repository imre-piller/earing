#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>

#include <assert.h>

#define MU_1 0
#define SIGMA_1 1
#define MU_2 2
#define SIGMA_2 3
#define MU_3 4
#define SIGMA_3 5
#define MU_4 6
#define SIGMA_4 7
#define ERROR 8

const int POPULATION_SIZE = 1000;

double genes[POPULATION_SIZE][9];

/**
 * Data array with [phi][chi] indices.
 */
double targets[72][19];

/**
 * Load data from the file to the global variable.
 */
void loadTargets(const std::string& path)
{
    std::ifstream csvFile(path);
    for (int j = 0; j < 19; ++j) {
        for (int i = 0; i < 72; ++i) {
            csvFile >> targets[i][j];
            if (targets[i][j] < 0.0) {
                targets[i][j] = 0.0;
            }
        }
    }
}

/**
 * Calculate the Gauss function.
 */
inline double gauss(double x, double mu, double sigma)
{
    return 1.0 / sqrt(2 * M_PI) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

/**
 * Solve the linear equation system.
 */
void solve(double a[4][4], double b[4], double* x)
{
    for (int k = 0; k < 3; ++k) {
        for (int i = k + 1; i < 4; ++i) {
            double gamma = a[i][k] / a[k][k];
            b[i] -= gamma * b[k];
            for (int j = k; j < 4; ++j) {
                a[i][j] -= gamma * a[k][j];
            }
        }
    }
    for (int i = 3; i >= 0; --i) {
        double s = 0.0;
        for (int k = i + 1; k < 4; ++k) {
            s += a[i][k] * x[k];
        }
        x[i] = (b[i] - s) / a[i][i];
    }
}

/**
 * Calculate optimal alpha values.
 */
void calc_alphas(int index, double* target, double* alphas)
{
    double a[4][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    double b[4] = {0, 0, 0, 0};
    for (int i = 0; i < 4; ++i) {
        double mu_i = genes[index][i * 2];
        double sigma_i = genes[index][i * 2 + 1];
        for (int k = 0; k < 4; ++k) {
            double mu_k = genes[index][k * 2];
            double sigma_k = genes[index][k * 2 + 1];
            for (int j = 0; j < 19; ++j) {
                double x = 5.0 * j;
                a[i][k] += gauss(x, mu_k, sigma_k) * gauss(x, mu_i, sigma_i);
            }
        }
        for (int j = 0; j < 19; ++j) {
            double x = 5.0 * j;
            b[i] += target[j] * gauss(x, mu_i, sigma_i);
        }
    }
    solve(a, b, alphas);
}

/**
 * Evaluate the given individual.
 */
void evaluate(int index, double* target)
{
    double s = 0.0;
    double alphas[4];
    calc_alphas(index, target, alphas);
    for (int j = 0; j < 19; ++j) {
        double mu_1 = genes[index][MU_1];
        double sigma_1 = genes[index][SIGMA_1];
        double mu_2 = genes[index][MU_2];
        double sigma_2 = genes[index][SIGMA_2];
        double mu_3 = genes[index][MU_3];
        double sigma_3 = genes[index][SIGMA_3];
        double mu_4 = genes[index][MU_4];
        double sigma_4 = genes[index][SIGMA_4];
        double x = 5.0 * j;
        double y = alphas[0] * gauss(x, mu_1, sigma_1)
                 + alphas[1] * gauss(x, mu_2, sigma_2)
                 + alphas[2] * gauss(x, mu_3, sigma_3)
                 + alphas[3] * gauss(x, mu_4, sigma_4);
        double diff = y - target[j];
        s += diff * diff;
    }
    genes[index][ERROR] = s;
}

/**
 * Generate random value on the [a, b) interval (a = 0, b = 1 by default).
 */
double unirand(double a = 0.0, double b = 1.0)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    double value = a + distribution(generator) * (b - a);
    return value;
}

/**
 * Create the initial population.
 */
void create_initial_population()
{
    for (int index = 0; index < POPULATION_SIZE; ++index) {
        genes[index][MU_1] = unirand(0, 10);
        genes[index][SIGMA_1] = 10;
        genes[index][MU_2] = unirand(20, 40);
        genes[index][SIGMA_2] = 10;
        genes[index][MU_3] = unirand(50, 70);
        genes[index][SIGMA_3] = 10;
        genes[index][MU_4] = unirand(80, 90);
        genes[index][SIGMA_4] = 10;
        evaluate(index, targets[0]);
        std::cout << "-- error: " << genes[index][ERROR] << std::endl;
    }
}

int main(int argc, char* argv[])
{
    loadTargets("data.txt");
    create_initial_population();
    return 0;
}

