#include "utils.h"

#include <fstream>
#include <math.h>
#include <random>

void loadTargets(const std::string& path, double targets[72][19])
{
    std::ifstream csvFile(path);
    for (int j = 0; j < 19; ++j) {
        for (int i = 0; i < 72; ++i) {
            csvFile >> targets[i][j];
            if (targets[i][j] < 0.0) {
                targets[i][j] = 0.0;
            }
        }
    }
}

double gauss(double x, double mu, double sigma)
{
    return (1.0 / (sqrt(2 * M_PI) * sigma)) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

void solve(double a[4][4], double b[4], double* x)
{
    for (int k = 0; k < 3; ++k) {
        for (int i = k + 1; i < 4; ++i) {
            double gamma = a[i][k] / a[k][k];
            b[i] -= gamma * b[k];
            for (int j = k; j < 4; ++j) {
                a[i][j] -= gamma * a[k][j];
            }
        }
    }
    for (int i = 3; i >= 0; --i) {
        double s = 0.0;
        for (int k = i + 1; k < 4; ++k) {
            s += a[i][k] * x[k];
        }
        x[i] = (b[i] - s) / a[i][i];
    }
}

void calc_alphas(double* gene, double* target, double* alphas)
{
    double a[4][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    double b[4] = {0, 0, 0, 0};
    for (int i = 0; i < 4; ++i) {
        double mu_i = gene[i * 2];
        double sigma_i = gene[i * 2 + 1];
        for (int k = 0; k < 4; ++k) {
            double mu_k = gene[k * 2];
            double sigma_k = gene[k * 2 + 1];
            for (int j = 0; j < 19; ++j) {
                double x = 5.0 * j;
                a[i][k] += gauss(x, mu_k, sigma_k) * gauss(x, mu_i, sigma_i);
            }
        }
        for (int j = 0; j < 19; ++j) {
            double x = 5.0 * j;
            b[i] += target[j] * gauss(x, mu_i, sigma_i);
        }
    }
    solve(a, b, alphas);
}

double unirand(double a, double b)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    double value = a + distribution(generator) * (b - a);
    return value;
}

int randint(int a, int b)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    int value = a + (int)(distribution(generator) * (b - a + 1));
    return value;
}
