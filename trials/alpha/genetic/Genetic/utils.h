#ifndef UTILS_H
#define UTILS_H

#include <string>

/**
 * Load data from the file to the global variable.
 */
void loadTargets(const std::string& path, double targets[72][19]);

/**
 * Calculate the Gauss function.
 */
double gauss(double x, double mu, double sigma);

/**
 * Solve the linear equation system.
 */
void solve(double a[4][4], double b[4], double* x);

/**
 * Calculate optimal alpha values.
 */
void calc_alphas(double* gene, double* target, double* alphas);

/**
 * Generate random value on the [a, b) interval (a = 0, b = 1 by default).
 */
double unirand(double a = 0.0, double b = 1.0);

/**
 * Generate random integer value on the [a, b] interval.
 */
int randint(int a, int b);

#endif // UTILS_H
