#include "population.h"

#include <QDebug>

#include <math.h>
#include "utils.h"

Population::Population()
{
}

void Population::setTarget(double* target)
{
    _target = target;
}

void Population::initialize()
{
    for (int index = 0; index < POPULATION_SIZE; ++index) {
        _genes[index][MU_1] = 0;
        _genes[index][SIGMA_1] = 5;
        _genes[index][MU_2] = 30;
        _genes[index][SIGMA_2] = 5;
        _genes[index][MU_3] = 60;
        _genes[index][SIGMA_3] = 5;
        _genes[index][MU_4] = 90;
        _genes[index][SIGMA_4] = 5;
        evaluate(index);
        double error = _genes[index][ERROR];
        Item item;
        item.index = index;
        item.value = error;
        _rank.add(item);
    }
}

void Population::showRank()
{
    _rank.print();
}

double Population::evolve()
{
    double bestError = _rank.get(0).value;
    double error = bestError;
    int nTrials = 0;
    while (error >= bestError) {
        int index = _rank.removeLast();
        int a = randint(0, 100);
        int b = randint(0, 100);
        do {
            cross(a, b, index);
            double strength = sqrt(nTrials);
            if (strength > 20.0) {
                strength = 20.0;
            }
            mutate(index, strength);
            evaluate(index);
            error = _genes[index][ERROR];
            // qDebug() << "trial " << nTrials;
        } while (error != error);
        // qDebug() << ">>" << error;
        Item item;
        item.index = index;
        item.value = error;
        _rank.add(item);
        ++nTrials;
        // qDebug() << "ranked" << error;
    }
    return error;
}

void Population::cross(int a, int b, int index)
{
    for (int i = 0; i < 8; ++i) {
        _genes[index][i] = _genes[a][i];
    }
    int k = randint(0, 3);
    double w = unirand(0.3, 0.7);
    switch (k) {
    case 0:
        _genes[index][MU_1] = _genes[a][MU_1] + w * (_genes[b][MU_1] - _genes[a][MU_1]);
        _genes[index][SIGMA_1] = _genes[a][SIGMA_1] + w * (_genes[b][SIGMA_1] - _genes[a][SIGMA_1]);
        break;
    case 1:
        _genes[index][MU_2] = _genes[a][MU_2] + w * (_genes[b][MU_2] - _genes[a][MU_2]);
        _genes[index][SIGMA_2] = _genes[a][SIGMA_2] + w * (_genes[b][SIGMA_2] - _genes[a][SIGMA_2]);
        break;
    case 2:
        _genes[index][MU_3] = _genes[a][MU_3] + w * (_genes[b][MU_3] - _genes[a][MU_3]);
        _genes[index][SIGMA_3] = _genes[a][SIGMA_3] + w * (_genes[b][SIGMA_3] - _genes[a][SIGMA_3]);
        break;
    case 3:
        _genes[index][MU_4] = _genes[a][MU_4] + w * (_genes[b][MU_4] - _genes[a][MU_4]);
        _genes[index][SIGMA_4] = _genes[a][SIGMA_4] + w * (_genes[b][SIGMA_4] - _genes[a][SIGMA_4]);
        break;
    }
}

void Population::mutate(int index, double strength)
{
    for (int i = 0; i < 8; ++i) {
        _genes[index][i] += unirand(-strength, strength);
        if (_genes[index][i] < 0.0) {
            _genes[index][i] = 0.0;
        }
        if (_genes[index][i] > 90.0) {
            _genes[index][i] = 90.0;
        }
    }
}

double Population::getBestError()
{
    return _rank.get(0).value;
}

std::vector<double> Population::getBestParams()
{
    std::vector<double> bestParams;
    int index = _rank.get(0).index;
    double alphas[4];
    calc_alphas(_genes[index], _target, alphas);
    bestParams.push_back(alphas[0]);
    bestParams.push_back(_genes[index][MU_1]);
    bestParams.push_back(_genes[index][SIGMA_1]);
    bestParams.push_back(alphas[1]);
    bestParams.push_back(_genes[index][MU_2]);
    bestParams.push_back(_genes[index][SIGMA_2]);
    bestParams.push_back(alphas[2]);
    bestParams.push_back(_genes[index][MU_3]);
    bestParams.push_back(_genes[index][SIGMA_3]);
    bestParams.push_back(alphas[3]);
    bestParams.push_back(_genes[index][MU_4]);
    bestParams.push_back(_genes[index][SIGMA_4]);
    return bestParams;
}

double Population::evaluate(int index)
{
    double s = 0.0;
    double alphas[4];
    calc_alphas(_genes[index], _target, alphas);
    for (int j = 0; j < 19; ++j) {
        double mu_1 = _genes[index][MU_1];
        double sigma_1 = _genes[index][SIGMA_1];
        double mu_2 = _genes[index][MU_2];
        double sigma_2 = _genes[index][SIGMA_2];
        double mu_3 = _genes[index][MU_3];
        double sigma_3 = _genes[index][SIGMA_3];
        double mu_4 = _genes[index][MU_4];
        double sigma_4 = _genes[index][SIGMA_4];
        double x = 5.0 * j;
        double y = alphas[0] * gauss(x, mu_1, sigma_1)
                 + alphas[1] * gauss(x, mu_2, sigma_2)
                 + alphas[2] * gauss(x, mu_3, sigma_3)
                 + alphas[3] * gauss(x, mu_4, sigma_4);
        double diff = y - _target[j];
        s += diff * diff;
    }
    _genes[index][ERROR] = s;
    return s;
}
