#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>

#include <QDebug>

#include "population.h"
#include "rank.h"
#include "utils.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    double targets[72][19];
    loadTargets("data.txt", targets);

    Population populations[72];
    Rank rank(false);

    // Initialization
    qDebug() << "Initialize ...";
    for (int i = 0; i < 72; ++i) {
        qDebug() << ">>" << i;
        populations[i].setTarget(targets[i]);
        populations[i].initialize();
        double error = populations[i].getBestError();
        Item item;
        item.index = i;
        item.value = error;
        rank.add(item);
    }

    // rank.print();

    // Search
    qDebug() << "Search ...";
    double error = 8888;
    while (error > 1.0) {
        int index = rank.removeFirst();
        error = rank.get(0).value;
        qDebug() << "[" << index << "] ::" << error;
        double decreasedError = error;
        while (decreasedError >= error) {
            decreasedError = populations[index].evolve();
        }
        Item item;
        item.index = index;
        item.value = decreasedError;
        rank.add(item);
    }

    // Save parameters
    std::ofstream paramFile;
    paramFile.open("params.csv");
    for (int i = 0; i < 72; ++i) {
        std::vector<double> params = populations[i].getBestParams();
        paramFile << i << " ";
        for (int j = 0; j < 12; ++j) {
            paramFile << params[j] << " ";
        }
        paramFile << std::endl;
    }
    paramFile.close();

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
