#ifndef POPULATION_H
#define POPULATION_H

#include <vector>

#include "rank.h"

#define MU_1 0
#define SIGMA_1 1
#define MU_2 2
#define SIGMA_2 3
#define MU_3 4
#define SIGMA_3 5
#define MU_4 6
#define SIGMA_4 7
#define ERROR 8

#define POPULATION_SIZE 1000

class Population
{
public:

    /**
     * Construct an empty population.
     */
    Population();

    /**
     * Set the target values of the population.
     */
    void setTarget(double* target);

    /**
     * Create the initial population.
     */
    void initialize();

    /**
     * Show the content of the rank.
     */
    void showRank();

    /**
     * Evolve the population while gaining a better individual.
     * @return error value of the best individual
     */
    double evolve();

    /**
     * Cross the given genes.
     * @param a index of the first gene
     * @param b index of the second gene
     * @param index index of the resulted gene
     */
    void cross(int a, int b, int index);

    /**
     * Mutate the given gene.
     * @param index index of the mutated gene
     * @param strength strength of the mutation operator
     */
    void mutate(int index, double strength);

    /**
     * Get the best error value of the population.
     */
    double getBestError();

    /**
     * Get the best parameters.
     */
    std::vector<double> getBestParams();

    /**
     * Evaluate the given individual.
     */
    double evaluate(int index);

private:

    double _genes[POPULATION_SIZE][9];
    double* _target;
    Rank _rank;
};

#endif // POPULATION_H
