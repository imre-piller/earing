#include "rank.h"

#include <iostream>

Rank::Rank(bool isIncreasing)
{
    _isIncreasing = isIncreasing;
}

Item Rank::get(int index)
{
    std::list<Item>::iterator it = _list.begin();
    for (int i = 0; i < index; ++i) {
        ++it;
    }
    return *it;
}

int Rank::size() const
{
    return _list.size();
}

void Rank::add(const Item& item)
{
    std::list<Item>::iterator it = _list.begin();
    if (_isIncreasing == true) {
        while (it != _list.end() && it->value < item.value) {
            ++it;
        }
    }
    else {
        while (it != _list.end() && it->value > item.value) {
            ++it;
        }
    }
    _list.insert(it, item);
}

void Rank::remove(int index)
{
    std::list<Item>::iterator it = _list.begin();
    while (it != _list.end() && it->index != index) {
        ++it;
    }
    if (it != _list.end()) {
        // TODO: Use exception instead!
        _list.erase(it);
    }
}

int Rank::removeLast()
{
    std::list<Item>::iterator it = _list.end();
    --it;
    // TODO: Check the case of empty list!
    int index = it->index;
    _list.erase(it);
    return index;
}

int Rank::removeFirst()
{
    std::list<Item>::iterator it = _list.begin();
    // TODO: Check the case of empty list!
    int index = it->index;
    _list.erase(it);
    return index;
}

void Rank::print() const
{
    for (const Item& item : _list) {
        std::cout << "[" << item.index << "] " << item.value << std::endl;
    }
}
