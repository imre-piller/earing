#!/bin/bash

rm genetic.exe
echo 'Compile ...'
g++ genetic.cpp -o genetic.exe -O3

rm -rf results
mkdir results
mkdir results/problems

echo 'Search parameters ...'
./genetic.exe > results/params.csv

echo 'Draw plots ...'
python plotter.py
python rose.py

