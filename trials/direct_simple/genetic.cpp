#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>

#include <assert.h>

const double CORRECTION = 2.5066282746310002;

const int N_GENERATIONS = 1000;
const int N_PREV_BEST_ITEMS = 300;
const int N_CROSSED_ITEMS = 500;
const int N_NEW_ITEMS = 200;

/**
 * Generate random value on the [a, b) interval (a = 0, b = 1 by default).
 */
double unirand(double a = 0.0, double b = 1.0)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    double value = a + distribution(generator) * (b - a);
    return value;
}

/**
 * Choose an index from [0, k) interval.
 */
int choose(int k)
{
    return k * unirand();
}

/**
 * Data array with [phi][chi] indices.
 */
double data[72][19];

/**
 * Load data from the file to the global variable.
 */
void loadData(const std::string& path)
{
    std::ifstream csvFile(path);
    for (int j = 0; j < 19; ++j) {
        for (int i = 0; i < 72; ++i) {
            csvFile >> data[i][j];
            if (data[i][j] < 0.0) {
                data[i][j] = 0.0;
            }
        }
    }
}

/**
 * Chromosome with the genes as real values
 */
class Chromosome
{
public:

    /**
     * Create a random chromosome.
     */
    Chromosome()
    {
        for (int i = 0; i < 12; ++i) {
            _genes[i] = unirand(0.0, 100.0);
        }
    }

    /**
     * Crossover the chromosomes in place.
     */
    void crossover(Chromosome& a, Chromosome& b, int nGenes)
    {
        for (int k = 0; k < nGenes; ++k) {
            int i = choose(12);
            double x1 = a._genes[i];
            double x2 = b._genes[i];
            double p = unirand();
            a._genes[i] = x1 + p * (x2 - x1);
            b._genes[i] = x2 - p * (x2 - x1);
        }
    }

    /**
     * Mutate the genes.
     */
    void mutate(int nGenes)
    {
        for (int k = 0; k < nGenes; ++k) {
            int i = choose(12);
            double noise = unirand(-10, 10);
            _genes[i] += noise;
        }
    }

    /**
     * Calculate the fitness value of the chromosome.
     */
    inline double calcFitness(int phiIndex) const
    {
        double distance = 0;
        double alpha1 = _genes[0];
        double mu1 = _genes[1];
        double sigma1 = _genes[2];
        double alpha2 = _genes[3];
        double mu2 = _genes[4];
        double sigma2 = _genes[5];
        double alpha3 = _genes[6];
        double mu3 = _genes[7];
        double sigma3 = _genes[8];
        double alpha4 = _genes[9];
        double mu4 = _genes[10];
        double sigma4 = _genes[11];

        for (int i = 0; i < 19; ++i) {
            double x = 5.0 * i;
            double d1 = (x - mu1) * (x - mu1);
            double v1 = (alpha1 / sigma1) * exp(-d1 / (2 * sigma1 * sigma1));
            double d2 = (x - mu2) * (x - mu2);
            double v2 = (alpha2 / sigma2) * exp(-d2 / (2 * sigma2 * sigma2));
            double d3 = (x - mu3) * (x - mu3);
            double v3 = (alpha3 / sigma3) * exp(-d3 / (2 * sigma3 * sigma3));
            double d4 = (x - mu4) * (x - mu4);
            double v4 = (alpha4 / sigma4) * exp(-d4 / (2 * sigma4 * sigma4));
            double t = (v1 + v2 + v3 + v4) - data[phiIndex][i];
            distance += t * t;
        }

        return distance;
    }

    /**
     * Regulate the chromosomes.
     */
    void regulate()
    {
        for (int i = 0; i < 12; i += 3) {
            // alpha
            if (_genes[i] < 0.0) {
                _genes[i] = 0.0;
            }
            // mu
            if (_genes[i + 1] < 0.0) {
                _genes[i + 1] = 0.0;
            }
            if (_genes[i + 1] > 90.0) {
                _genes[i + 1] = 90.0;
            }
            // sigma
            if (_genes[i + 2] < 1.0) {
                _genes[i + 2] = 1.0;
            }
        }
    }

    /**
     * Print the chromosome.
     */
    void print()
    {
        std::cout << "(" << _genes[0] << ", " << _genes[1] << ", " << _genes[2] << ")" << std::endl;
        std::cout << "(" << _genes[3] << ", " << _genes[4] << ", " << _genes[5] << ")" << std::endl;
        std::cout << "(" << _genes[6] << ", " << _genes[7] << ", " << _genes[8] << ")" << std::endl;
        std::cout << "(" << _genes[9] << ", " << _genes[10] << ", " << _genes[11] << ")" << std::endl;
    }

    /**
     * Print the row of the result table.
     */
    void printRow(int phiIndex)
    {
        double area1 = _genes[0] * CORRECTION;
        double center1 = _genes[1];
        double area2 = _genes[3] * CORRECTION;
        double center2 = _genes[4];
        double area3 = _genes[6] * CORRECTION;
        double center3 = _genes[7];
        double area4 = _genes[9] * CORRECTION;
        double center4 = _genes[10];
        double result = area1 * center1 + area2 * center2 + area3 * center3 + area4 * center4;
        std::cout <<
             phiIndex * 5 << " " <<
             area1 << " " << center1 << " " <<
             area2 << " " << center2 << " " <<
             area3 << " " << center3 << " " <<
             area4 << " " << center4 << " " << result << std::endl;
    }

    /**
     * Print the parameters of the Gauss functions.
     */
    void printGaussParameters(int phiIndex)
    {
        std::cout << phiIndex << " ";
        for (int i = 0; i < 12; ++i) {
            std::cout << _genes[i] << " ";
        }
        std::cout << std::endl;
    }

private:
    double _genes[12];
};

/**
 * Genetic algoritmic search
 */
void search()
{
    for (int phiIndex = 0; phiIndex < 72; ++phiIndex) {
        std::vector<Chromosome> population(1000);
        for (int i = 0; i < 1000; ++i) {
            population[i].regulate();
        }
        std::vector<std::pair<double, int>> fitness;

        for (int generation = 0; generation < N_GENERATIONS; ++generation) {

            fitness.clear();
            for (int i = 0; i < N_GENERATIONS; ++i) {
                std::pair<double, int> item(population[i].calcFitness(phiIndex), i);
                fitness.push_back(item);
            }
            std::sort(fitness.begin(), fitness.end());

            std::vector<Chromosome> newPopulation;

            // Previous best items
            for (int i = 0; i < N_PREV_BEST_ITEMS; ++i) {
                newPopulation.push_back(population[fitness[i].second]);
            }

            // Crossed items
            for (int i = 0; i < N_CROSSED_ITEMS / 2; ++i) {
                int j = choose(N_PREV_BEST_ITEMS);
                Chromosome a = newPopulation[j];
                j = choose(N_PREV_BEST_ITEMS);
                Chromosome b = newPopulation[j];
                a.crossover(a, b, 4);
                a.mutate(3);
                a.regulate();
                b.mutate(3);
                b.regulate();
                newPopulation.push_back(a);
                newPopulation.push_back(b);
            }

            // New items
            for (int i = 0; i < N_NEW_ITEMS; ++i) {
                Chromosome c;
                c.regulate();
                newPopulation.push_back(c);
            }

            population = newPopulation;
        }
        population[fitness[0].second].printGaussParameters(phiIndex);
    }
}

/**
 * Genetic algoritm for curve fitting.
 */
int main(int argc, char* argv[])
{
    loadData("../../samples/H17_HA.txt");
    search();
    return 0;
}

