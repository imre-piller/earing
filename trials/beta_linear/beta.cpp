#include <iostream>

void solve(double a[4][4], double b[4], double* x)
{
    for (int k = 0; k < 3; ++k) {
        for (int i = k + 1; i < 4; ++i) {
            double gamma = a[i][k] / a[k][k];
            b[i] -= gamma * b[k];
            for (int j = k; j < 4; ++j) {
                a[i][j] -= gamma * a[k][j];
            }
        }
    }
    for (int i = 3; i >= 0; --i) {
        double s = 0.0;
        for (int k = i + 1; k < 4; ++k) {
            s += a[i][k] * x[k];
        }
        x[i] = (b[i] - s) / a[i][i];
    }
}

int main(int argc, char* argv[])
{
    double a[4][4] = {
        {10, 13, 8, -30},
        {15, -9, -7, 4},
        {5, -88, 10, 8},
        {32, -71, 20, 0}
    };
    double b[4] = {-123, 439, 100, 80};
    double x[4];
    solve(a, b, x);
    std::cout << "== Solution" << std::endl;
    for (int i = 0; i < 4; ++i) {
        std::cout << x[i] << std::endl;
    }
    return 0;
}
