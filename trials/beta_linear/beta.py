import copy
import csv
import math
import random
import numpy as np


def load_samples(path):
    """
    Load the sample of measurements.
    """
    samples = [[] for _ in range(72)]
    with open(path) as sample_file:
        csv_reader = csv.reader(sample_file, delimiter=' ')
        for row in csv_reader:
            for j in range(72):
                samples[j].append(float(row[j]))
    return samples


def g(x, mu, sigma):
    """Gauss-like function."""
    return 1.0 / (math.sqrt(2 * math.pi) * sigma) * math.exp(-((x - mu)*(x - mu))/(2 * sigma*sigma))


def calc_optimal_betas(genes, target):
    """Calculate the optimal beta values."""
    a = np.zeros((4, 4))
    b = np.zeros((4, 1))
    for i in range(4):
        for k in range(4):
            for j in range(19):
                x = 5 * j
                a[i, k] += g(x, **genes[k]) * g(x, **genes[i])
        for j in range(19):
            x = 5 * j
            b[i] += target[j] * g(x, **genes[i])
    betas = np.linalg.solve(a, b)
    return betas


def create_new_population(n):
    individual = {
        'genes': [
            {'mu':  0, 'sigma': 10},
            {'mu': 30, 'sigma': 10},
            {'mu': 60, 'sigma': 10},
            {'mu': 90, 'sigma': 10}
        ]
    }
    return [individual.copy() for _ in range(n)]


def calc_approximation(genes, target):
    betas = calc_optimal_betas(genes, target)
    approximation = [0.0] * 91
    j = 0
    for gene in genes:
        for i in range(91):
            value = betas[j] * g(i, **gene)
            approximation[i] += value
        j += 1
    return approximation


def calc_errors(population, target):
    for i in range(len(population)):
        approximation = calc_approximation(population[i]['genes'], target)
        diff = (np.array(approximation) - np.array(target)) ** 2
        population[i]['mean_error'] = np.mean(diff)
        population[i]['max_error'] = np.max(diff)


def create_mutated(population, n):
    mutated = []
    for _ in range(n):
        individual = copy.deepcopy(random.choice(population))
        individual.pop('mean_error', None)
        individual.pop('max_error', None)
        k = random.randint(0, 3)
        mu = individual['genes'][k]['mu']
        sigma = individual['genes'][k]['sigma']
        mu += random.random() * 20 - 10
        sigma += random.random() * 20 - 10
        if mu < 0:
            mu = 0
        if mu > 90:
            mu = 90
        if sigma < 1:
            sigma = 1
        individual['genes'][k]['mu'] = mu
        individual['genes'][k]['sigma'] = sigma
        mutated.append(individual)
    return mutated


population_size = 50
mutation_size = 50

samples = load_samples('../../samples/H17_HA.txt')
populations = [create_new_population(population_size) for _ in range(72)]
best_errors = [None] * 72

print('Initialize ...')
for i in range(len(populations)):
    calc_errors(populations[i], samples[i])
    populations[i].sort(key=lambda x: x['max_error'])
    best_errors[i] = populations[i][0]['max_error']

print('Search ...')
k = 0
while max(best_errors) > 0.4:
    mutated = create_mutated(populations[k], mutation_size)
    calc_errors(mutated, samples[k])
    populations[k] += mutated
    populations[k].sort(key=lambda x: x['max_error'])
    populations[k] = populations[k][:population_size]
    best_errors[k] = populations[k][0]['max_error']
    k = np.argmax(best_errors)
    print('k = {}, error = {}'.format(k, max(best_errors)))

