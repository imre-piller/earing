#include "population.h"

#include <QDebug>

#include <math.h>
#include "utils.h"

Population::Population()
{
}

void Population::setTarget(double* target)
{
    _targets = target;
}

void Population::initialize()
{
    double alphas[4];
    double actuals[19];
    for (int index = 0; index < POPULATION_SIZE; ++index) {
        bool hasValidAlphas = false;
        while (hasValidAlphas == false) {
            _genes[index][MU_1] = unirand(0, 20);
            _genes[index][SIGMA_1] = unirand(4, 10);
            _genes[index][MU_2] = unirand(20, 50);
            _genes[index][SIGMA_2] = unirand(4, 10);
            _genes[index][MU_3] = unirand(50, 80);
            _genes[index][SIGMA_3] = unirand(4, 10);
            _genes[index][MU_4] = unirand(80, 90);
            _genes[index][SIGMA_4] = unirand(4, 10);

            calc_alphas(_genes[index], _targets, alphas);
            hasValidAlphas = true;
            for (int k = 0; k < 4; ++k) {
                if (alphas[k] < 0.0) {
                    hasValidAlphas = false;
                }
            }
        }
        calc_actuals(_genes[index], alphas, actuals);
        double sse = calc_sse(actuals, _targets, 19);
        _genes[index][ERROR] = sse;
        _rank.add(index, sse);
    }
}

void Population::showRank()
{
    _rank.print();
}

double Population::evolve()
{
    double bestError = _rank.get(0).value;
    double worstError = _rank.get(_rank.size() - 1).value;
    double error = worstError;
    double alphas[4];
    double actuals[19];
    while (error >= worstError) {
        int index = _rank.removeLast();
        int a = randint(0, 100);
        int b = randint(0, 300);
        cross(a, b, index);
        double strength = _genes[b][ERROR] * 5.0;
        if (strength > 5.0) {
            strength = 5.0;
        }
        mutate(index, strength);
        calc_alphas(_genes[index], _targets, alphas);
        if (alphas[0] != alphas[0] || alphas[1] != alphas[1] || alphas[2] != alphas[2] || alphas[3] != alphas[3]) {
            for (int j = 0; j < 4; ++j) {
                qDebug() << "alpha" << j << alphas[j];
            }
            int i = randint(0, 500);
            while (i == index) {
                i = randint(0, 500);
            }
            for (int k = 0; k < 8; ++k) {
                _genes[index][k] = _genes[i][k];
            }
            _rank.add(index, error);
            continue;
        }
        if (alphas[0] < 0.0 || alphas[1] < 0.0 || alphas[2] < 0.0 || alphas[3] < 0.0) {
            int i = randint(0, 500);
            while (i == index) {
                i = randint(0, 500);
            }
            for (int k = 0; k < 8; ++k) {
                _genes[index][k] = _genes[i][k];
            }
            _rank.add(index, error);
            continue;
        }
        order(index);
        calc_actuals(_genes[index], alphas, actuals);
        error = calc_sse(actuals, _targets, 19);
        _genes[index][ERROR] = error;
        _rank.add(index, error);
    }
    return error;
}

void Population::cross(int a, int b, int index)
{
    for (int i = 0; i < 8; ++i) {
        _genes[index][i] = _genes[a][i];
        // _genes[index2][i] = _genes[b][i];
    }
    int k = randint(0, 3) * 2;
    _genes[index][k] = _genes[b][k];
    _genes[index][k + 1] = _genes[b][k + 1];
    // _genes[index2][k] = _genes[a][k];
    // _genes[index2][k + 1] = _genes[a][k + 1];
}

void Population::mutate(int index, double strength)
{
    for (int i = 0; i < 8; ++i) {
        double noise = unirand(-strength, strength);
        _genes[index][i] += noise;
        if (_genes[index][i] < 0.0) {
            _genes[index][i] = 0.0;
        }
        if (_genes[index][i] > 90.0) {
            _genes[index][i] = 90.0;
        }
        if (i % 2 == 1 && _genes[index][i] < 3.0) {
            _genes[index][i] = unirand(3, 7);
        }
    }
}

void Population::order(int index)
{
    double mu;
    double sigma;
    double* gene = _genes[index];
    for (int i = 0; i < 8; i += 2) {
        int minIndex = i;
        for (int k = i + 2; k < 8; k += 2) {
            if (gene[k] < gene[minIndex]) {
                minIndex = k;
            }
        }
        if (i != minIndex) {
            mu = gene[minIndex];
            gene[minIndex] = gene[i];
            gene[i] = mu;
            sigma = gene[minIndex + 1];
            gene[minIndex + 1] = gene[i + 1];
            gene[i + 1] = sigma;
        }
    }
}

double Population::getBestError()
{
    return _rank.get(0).value;
}

std::vector<double> Population::getBestParams()
{
    std::vector<double> bestParams;
    int index = _rank.get(0).index;
    double alphas[4];
    calc_alphas(_genes[index], _targets, alphas);
    bestParams.push_back(alphas[0]);
    bestParams.push_back(_genes[index][MU_1]);
    bestParams.push_back(_genes[index][SIGMA_1]);
    bestParams.push_back(alphas[1]);
    bestParams.push_back(_genes[index][MU_2]);
    bestParams.push_back(_genes[index][SIGMA_2]);
    bestParams.push_back(alphas[2]);
    bestParams.push_back(_genes[index][MU_3]);
    bestParams.push_back(_genes[index][SIGMA_3]);
    bestParams.push_back(alphas[3]);
    bestParams.push_back(_genes[index][MU_4]);
    bestParams.push_back(_genes[index][SIGMA_4]);
    return bestParams;
}

double Population::evaluate(int index)
{
    double alphas[4];
    double actuals[19];
    double sse;
    calc_alphas(_genes[index], _targets, alphas);
    calc_actuals(_genes[index], alphas, actuals);
    sse = calc_sse(actuals, _targets, 19);
    _genes[index][ERROR] = sse;
    return sse;
}
