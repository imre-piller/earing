#include "utils.h"

#include <exception>
#include <fstream>
#include <math.h>
#include <random>

#include <QDebug>

void loadTargets(const std::string& path, double targets[72][19])
{
    std::ifstream csvFile(path);
    if (csvFile.is_open() == false) {
        throw std::invalid_argument("Unable to find the data.txt file!");
    }
    for (int j = 0; j < 19; ++j) {
        for (int i = 0; i < 72; ++i) {
            csvFile >> targets[i][j];
            if (targets[i][j] < 0.0) {
                targets[i][j] = 0.0;
            }
        }
    }
}

double gauss(double x, double mu, double sigma)
{
    return (1.0 / (sqrt(2 * M_PI) * sigma)) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

void solve(double a[4][4], double b[4], double* x)
{
    for (int k = 0; k < 3; ++k) {
        for (int i = k + 1; i < 4; ++i) {
            double gamma = a[i][k] / a[k][k];
            b[i] -= gamma * b[k];
            for (int j = k; j < 4; ++j) {
                a[i][j] -= gamma * a[k][j];
            }
        }
    }
    for (int i = 3; i >= 0; --i) {
        double s = 0.0;
        for (int k = i + 1; k < 4; ++k) {
            s += a[i][k] * x[k];
        }
        x[i] = (b[i] - s) / a[i][i];
    }
}

void calc_alphas(double* gene, double* target, double* alphas)
{
    double a[4][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    double b[4] = {0, 0, 0, 0};
    for (int i = 0; i < 4; ++i) {
        double mu_i = gene[i * 2];
        double sigma_i = gene[i * 2 + 1];
        for (int k = 0; k < 4; ++k) {
            double mu_k = gene[k * 2];
            double sigma_k = gene[k * 2 + 1];
            for (int j = 0; j < 19; ++j) {
                double x = 5.0 * j;
                a[i][k] += gauss(x, mu_k, sigma_k) * gauss(x, mu_i, sigma_i);
            }
        }
        for (int j = 0; j < 19; ++j) {
            double x = 5.0 * j;
            b[i] += target[j] * gauss(x, mu_i, sigma_i);
        }
    }
    solve(a, b, alphas);
}

void calc_actuals(double* gene, double* alphas, double* actuals)
{
    for (int j = 0; j < 19; ++j) {
        double mu_1 = gene[MU_1];
        double sigma_1 = gene[SIGMA_1];
        double mu_2 = gene[MU_2];
        double sigma_2 = gene[SIGMA_2];
        double mu_3 = gene[MU_3];
        double sigma_3 = gene[SIGMA_3];
        double mu_4 = gene[MU_4];
        double sigma_4 = gene[SIGMA_4];
        double x = 5.0 * j;
        double y = alphas[0] * gauss(x, mu_1, sigma_1)
                 + alphas[1] * gauss(x, mu_2, sigma_2)
                 + alphas[2] * gauss(x, mu_3, sigma_3)
                 + alphas[3] * gauss(x, mu_4, sigma_4);
        if (y != y) {
            qDebug() << "???";
            for (int i = 0; i < 8; ++i) {
                qDebug() << "[" << i << "]" << gene[i];
            }
        }
        actuals[j] = y;
    }
}

double calc_sse(double* actual, double* target, int n)
{
    double sse = 0.0;
    for (int j = 0; j < n; ++j) {
        double diff = actual[j] - target[j];
        sse += diff * diff;
    }
    return sse;
}

double unirand(double a, double b)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    double value = a + distribution(generator) * (b - a);
    return value;
}

int randint(int a, int b)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    int value = a + (int)(distribution(generator) * (b - a + 1));
    return value;
}

int choose(int k)
{
    return k * unirand();
}
