#ifndef CHROMOSOME_H
#define CHROMOSOME_H

const double CORRECTION = 2.5066282746310002;

// const int N_GENERATIONS = 1000;
const int POPULATION_SIZE = 1000;
const int N_GENERATIONS = 2000;
const int N_PREV_BEST_ITEMS = 400;
const int N_CROSSED_ITEMS = 500;
const int N_NEW_ITEMS = 100;

/**
 * Chromosome with the genes as real values
 */
class Chromosome
{
public:

    /**
     * Create a random chromosome.
     */
    Chromosome();

    /**
     * Crossover the chromosomes in place.
     */
    void crossover(Chromosome& a, Chromosome& b, int nGenes);

    /**
     * Mutate the genes.
     */
    void mutate(int nGenes);

    /**
     * Calculate the fitness value of the chromosome.
     */
    double calcFitness() const;

    /**
     * Regulate the chromosomes.
     */
    void regulate();

    /**
     * Print the row of the result table.
     */
    void printRow(int phiIndex);

    double const* getGenes() const;

    double calcRose() const;

    double* _targets;

private:

    double _genes[12];
};

#endif // CHROMOSOME_H
