#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "estimator.h"

#include <QMainWindow>
#include <QVector>

QString extractNameFromPath(const QString& path);

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    void openFiles();
    void findParameters();

private:
    Ui::MainWindow *ui;

    QVector<Estimator> _estimators;

    void loadResFiles();
    void startEstimation();
};

#endif // MAINWINDOW_H
