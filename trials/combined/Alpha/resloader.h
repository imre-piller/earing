#ifndef RESLOADER_H
#define RESLOADER_H

#include <QVector>
#include <QString>

namespace res
{
    /**
     * @brief Read data from the RES file.
     * @param path path of the RES file
     * @param targets target matrix
     */
    void readData(const QString& path, double targets[72][19]);

    /**
     * @brief Read the lines of the file to a vector as strings.
     * @param path path of the RES file
     * @return vector of strings
     */
    QVector<QString> readLines(const QString& path);

    /**
     * @brief Find the index of the first line which should be considered.
     * @param lines vector of strings
     * @return index of the first line of interesting data
     */
    int findFirstLine(const QVector<QString>& lines);

    /**
     * @brief Fill the data in the target matrix.
     * @param lines lines vector of strings
     * @param index index of the first line of interesting data
     * @param targets target matrix
     */
    void fillData(const QVector<QString>& lines, int index, double targets[72][19]);

    /**
     * @brief Read the row values from the given index.
     * @param lines vector of strings
     * @param index first line of the data row
     * @return values at the given row.
     */
    QVector<double> readRowValues(const QVector<QString>& lines, int index);

    /**
     * @brief Fill the phi0 values
     * @param phi0 the missing value from the table
     * @param targets measurement table
     */
    void fillPhiZero(double phi0, double targets[72][19]);
}

#endif // RESLOADER_H
