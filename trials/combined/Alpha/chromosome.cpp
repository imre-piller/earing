#include "chromosome.h"

#include "utils.h"

#include <math.h>
#include <iostream>

Chromosome::Chromosome()
{
    for (int i = 0; i < 12; ++i) {
        _genes[i] = 10.0;
    }
    static int modus = 0;
    // mu
    if (modus % 2 == 0) {
        _genes[1]  = 0.0;
        _genes[4]  = 30.0;
        _genes[7]  = 60.0;
        _genes[10] = 90.0;
    }
    else {
        _genes[1]  = 5.0;
        _genes[4]  = 15.0;
        _genes[7]  = 45.0;
        _genes[10] = 75.0;
    }
    ++modus;
}

void Chromosome::crossover(Chromosome& a, Chromosome& b, int nGenes)
{
    for (int k = 0; k < nGenes; ++k) {
        int i = choose(12);
        double x1 = a._genes[i];
        double x2 = b._genes[i];
        double p = unirand();
        a._genes[i] = x1 + p * (x2 - x1);
        b._genes[i] = x2 - p * (x2 - x1);
    }
}

void Chromosome::mutate(int nGenes)
{
    for (int k = 0; k < nGenes; ++k) {
        int i = choose(12);
        double noise = unirand(-10, 10);
        _genes[i] += noise;
    }
}

double Chromosome::calcFitness() const
{
    double distance = 0;
    double alpha1 = _genes[0];
    double mu1 = _genes[1];
    double sigma1 = _genes[2];
    double alpha2 = _genes[3];
    double mu2 = _genes[4];
    double sigma2 = _genes[5];
    double alpha3 = _genes[6];
    double mu3 = _genes[7];
    double sigma3 = _genes[8];
    double alpha4 = _genes[9];
    double mu4 = _genes[10];
    double sigma4 = _genes[11];

    for (int i = 0; i < 19; ++i) {
        double x = 5.0 * i;
        double d1 = (x - mu1) * (x - mu1);
        double v1 = (alpha1 / sigma1) * exp(-d1 / (2 * sigma1 * sigma1));
        double d2 = (x - mu2) * (x - mu2);
        double v2 = (alpha2 / sigma2) * exp(-d2 / (2 * sigma2 * sigma2));
        double d3 = (x - mu3) * (x - mu3);
        double v3 = (alpha3 / sigma3) * exp(-d3 / (2 * sigma3 * sigma3));
        double d4 = (x - mu4) * (x - mu4);
        double v4 = (alpha4 / sigma4) * exp(-d4 / (2 * sigma4 * sigma4));
        double t = (v1 + v2 + v3 + v4) - _targets[i];
        distance += t * t;
    }

    return distance;
}

void Chromosome::regulate()
{
    for (int i = 0; i < 12; i += 3) {
        // alpha
        if (_genes[i] < 0.0) {
            _genes[i] = 0.0;
        }
        // mu
        if (_genes[i + 1] < 0.0) {
            _genes[i + 1] = 0.0;
        }
        if (_genes[i + 1] > 90.0) {
            _genes[i + 1] = 90.0;
        }
        // sigma
        if (_genes[i + 2] < 1.0) {
            _genes[i + 2] = 1.0;
        }
    }
}

void Chromosome::printRow(int phiIndex)
{
    double area1 = _genes[0] * CORRECTION;
    double center1 = _genes[1];
    double area2 = _genes[3] * CORRECTION;
    double center2 = _genes[4];
    double area3 = _genes[6] * CORRECTION;
    double center3 = _genes[7];
    double area4 = _genes[9] * CORRECTION;
    double center4 = _genes[10];
    double result = area1 * center1 + area2 * center2 + area3 * center3 + area4 * center4;
    std::cout <<
         phiIndex * 5 << " " <<
         area1 << " " << center1 << " " <<
         area2 << " " << center2 << " " <<
         area3 << " " << center3 << " " <<
                 area4 << " " << center4 << " " << result << std::endl;
}

double const* Chromosome::getGenes() const
{
    return _genes;
}

double Chromosome::calcRose() const
{
    double area1 = _genes[0] * CORRECTION;
    double center1 = _genes[1];
    double area2 = _genes[3] * CORRECTION;
    double center2 = _genes[4];
    double area3 = _genes[6] * CORRECTION;
    double center3 = _genes[7];
    double area4 = _genes[9] * CORRECTION;
    double center4 = _genes[10];
    double result = area1 * center1 + area2 * center2 + area3 * center3 + area4 * center4;
    return result;
}

/*
void printGaussParameters(int phiIndex)
{
    std::cout << phiIndex << " ";
    for (int i = 0; i < 12; ++i) {
        std::cout << _genes[i] << " ";
    }
    std::cout << std::endl;
}
*/
