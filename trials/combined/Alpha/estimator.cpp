#include "estimator.h"


#include "utils.h"

#include <algorithm>
#include <fstream>
#include <vector>

#include <QDebug>

Estimator::Estimator()
{
}

void Estimator::search()
{
    for (int phiIndex = 0; phiIndex < 72; ++phiIndex) {
        std::vector<Chromosome> population(POPULATION_SIZE);
        for (int i = 0; i < POPULATION_SIZE; ++i) {
            population[i]._targets = _data[phiIndex];
            population[i].regulate();
        }
        std::vector<std::pair<double, int>> fitness;

        for (int generation = 0; generation < N_GENERATIONS; ++generation) {
            fitness.clear();
            for (int i = 0; i < POPULATION_SIZE; ++i) {
                std::pair<double, int> item(population[i].calcFitness(), i);
                fitness.push_back(item);
            }

            std::sort(fitness.begin(), fitness.end());

            std::vector<Chromosome> newPopulation;

            // Previous best items
            for (int i = 0; i < N_PREV_BEST_ITEMS; ++i) {
                newPopulation.push_back(population[fitness[i].second]);
            }

            // Crossed items
            for (int i = 0; i < N_CROSSED_ITEMS / 2; ++i) {
                int j = choose(N_PREV_BEST_ITEMS);
                Chromosome a = newPopulation[j];
                j = choose(N_PREV_BEST_ITEMS);
                Chromosome b = newPopulation[j];
                a.crossover(a, b, 4);
                a.mutate(3);
                a.regulate();
                b.mutate(3);
                b.regulate();
                newPopulation.push_back(a);
                newPopulation.push_back(b);
            }

            // New items
            for (int i = 0; i < N_NEW_ITEMS; ++i) {
                Chromosome c;
                c._targets = _data[phiIndex];
                c.regulate();
                newPopulation.push_back(c);
            }

            population = newPopulation;
        }
        // Save the best chromosome
        _bestChromosomes.push_back(population[fitness[0].second]);
        // _bestChromosome.printRow(phiIndex);
    }
}

double Estimator::calcMeanError() const
{
    double s = 0.0;
    for (const Chromosome& chromosome : _bestChromosomes) {
        s += chromosome.calcFitness();
    }
    double meanError = s / _bestChromosomes.size();
    return meanError;
}

double Estimator::calcMaxError() const
{
    double maxError = 0.0;
    for (const Chromosome& chromosome : _bestChromosomes) {
        double error = chromosome.calcFitness();
        if (error > maxError) {
            maxError = error;
        }
    }
    return maxError;
}

void Estimator::saveParams() const
{
    QString path = _name + "_params.csv";
    std::ofstream paramFile;
    paramFile.open(path.toStdString());
    for (const Chromosome& chromosome : _bestChromosomes) {
        double const* genes = chromosome.getGenes();
        for (int i = 0; i < 11; ++i) {
            paramFile << genes[i] << " ";
        }
        paramFile << genes[11] << std::endl;
    }
    paramFile.close();
}

void Estimator::saveErrors() const
{
    QString path = _name + "_errors.csv";
    std::ofstream errorFile;
    errorFile.open(path.toStdString());
    for (const Chromosome& chromosome : _bestChromosomes) {
        errorFile << chromosome.calcFitness() << std::endl;
    }
    errorFile.close();
}

void Estimator::saveRose() const
{
    QString path = _name + "_rose.csv";
    std::ofstream roseFile;
    roseFile.open(path.toStdString());
    for (const Chromosome& chromosome : _bestChromosomes) {
        roseFile << chromosome.calcRose() << std::endl;
    }
    roseFile.close();
}

void Estimator::saveElapsedTime(double elapsedTime) const
{
    QString path = _name + "_time.csv";
    std::ofstream timeFile;
    timeFile.open(path.toStdString());
    timeFile << elapsedTime << std::endl;
    timeFile.close();
}
