#ifndef RANK_H
#define RANK_H

#include <list>

struct Item
{
    int index;
    double value;
};

/**
 * Ordered item container class
 */
class Rank
{
public:

    /**
     * Construct an empty rank.
     */
    Rank(bool isIncreasing = true);

    /**
     * Get the n-th item from the rank.
     */
    Item get(int index);

    /**
     * Get the size of the rank.
     */
    int size() const;

    /**
     * Add new item to the rank.
     */
    void add(const Item& item);

    /**
     * Add new item to the rank.
     */
    void add(int index, double value);

    /**
     * Remove item from the rank by item index.
     */
    void remove(int index);

    /**
     * Remove the last element of the rank.
     * @return index of the removed element
     */
    int removeLast();

    /**
     * Remove the first element of the rank.
     * @return index of the removed element
     */
    int removeFirst();

    /**
     * Print the content of the rank.
     */
    void print() const;

private:

    std::list<Item> _list;
    bool _isIncreasing;

};

#endif // RANK_H
