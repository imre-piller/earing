#ifndef ESTIMATOR_H
#define ESTIMATOR_H

#include "chromosome.h"

#include <QString>
#include <QVector>

class Estimator
{
public:
    Estimator();

    void search();

    double calcMeanError() const;

    double calcMaxError() const;

    void saveParams() const;
    void saveErrors() const;
    void saveRose() const;
    void saveElapsedTime(double elapsedTime) const;

    double _data[72][19];
    QString _name;
    QVector<Chromosome> _bestChromosomes;
};

#endif // ESTIMATOR_H
