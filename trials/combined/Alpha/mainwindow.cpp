#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>

#include <QDebug>

#include <QColor>
#include <QDir>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QFileInfo>
#include <QTableWidgetItem>

#include "population.h"
#include "rank.h"
#include "resloader.h"
#include "utils.h"

QString extractNameFromPath(const QString& path)
{
    /*
    QFileInfo fileInfo(path);
    QString fileName = fileInfo.fileName();
    */
    if (path.endsWith(".RES")) {
        QString name = path;
        name.chop(4);
        return name;
    }
    else {
        throw std::invalid_argument("Invalid path!");
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    /*
    loadTargets("data.txt", targets);

    Population populations[72];
    Rank rank(false);

    double initialErrors[72];
    */

    /*
    res::readData("/tmp/first.RES", targets);
    for (int k = 0; k < 72; ++k) {
        qDebug() << k << targets[k][3];
    }
    */

    /*
    // Initialization
    qDebug() << "Initialize ...";
    for (int i = 0; i < 72; ++i) {
        qDebug() << ">>" << i;
        populations[i].setTarget(targets[i]);
        populations[i].initialize();
        double error = populations[i].getBestError();
        qDebug() << "::" << error;
        initialErrors[i] = error;
    }

    // 2000 iteration phase
    qDebug() << "2000 iteration phase ...";
    for (int i = 0; i < 72; ++i) {
        qDebug() << ">>" << i;
        for (int j = 0; j < 2000; ++j) {
            // qDebug() << "..." << j;
            populations[i].evolve();
        }
        double error = populations[i].getBestError();
        qDebug() << initialErrors[i] << "--->" << error;
        rank.add(i, error);
    }

    // Save parameters
    std::ofstream paramFile;
    paramFile.open("params.csv");
    for (int i = 0; i < 72; ++i) {
        std::vector<double> params = populations[i].getBestParams();
        paramFile << i << " ";
        for (int j = 0; j < 12; ++j) {
            paramFile << params[j] << " ";
        }
        paramFile << std::endl;
    }
    paramFile.close();
    */

    ui->setupUi(this);

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(5);
    QStringList labels = {"file", "phi 0", "mean error", "max error", "elapsed time"};
    ui->tableWidget->setHorizontalHeaderLabels(labels);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFiles()
{
    QStringList filenames = QFileDialog::getOpenFileNames(
        this,
        "Open RES file",
        QDir::currentPath(),
        "RES files (*.RES) ;; All files (*.*)"
    );

    if (filenames.size() > 0) {
        int rowCount = ui->tableWidget->rowCount();
        int newRowCount = rowCount + filenames.size();
        ui->tableWidget->setRowCount(newRowCount);
        for (int i = 0; i < filenames.size(); ++i) {
            QTableWidgetItem* pathItem = new QTableWidgetItem(filenames[i]);
            ui->tableWidget->setItem(rowCount + i, 0, pathItem);
            QTableWidgetItem* valueItem = new QTableWidgetItem("0.0");
            ui->tableWidget->setItem(rowCount + i, 1, valueItem);
        }
    }

    // ui->tableWidget->item(1, 0)->setBackgroundColor(QColor(200, 250, 200));
}

void MainWindow::findParameters()
{
    loadResFiles();
    startEstimation();
}

void MainWindow::loadResFiles()
{
    int nRows = ui->tableWidget->rowCount();
    for (int i = 0; i < nRows; ++i) {
        QString path = ui->tableWidget->item(i, 0)->data(Qt::DisplayRole).toString();
        Estimator estimator;
        estimator._name = extractNameFromPath(path);
        res::readData(path, estimator._data);

        double phi0 = ui->tableWidget->item(i, 1)->data(Qt::DisplayRole).toDouble();
        res::fillPhiZero(phi0, estimator._data);

        _estimators.append(estimator);
        qDebug() << i << path;
    }
}

void MainWindow::startEstimation()
{
    QElapsedTimer timer;
    for (int i = 0; i < _estimators.size(); ++i) {
        qDebug() << "Estimate" << i << "...";
        timer.start();
        _estimators[i].search();
        ui->tableWidget->item(i, 0)->setBackgroundColor(QColor(200, 250, 200));
        QTableWidgetItem* meanErrorItem = new QTableWidgetItem(QString::number(_estimators[i].calcMeanError()));
        ui->tableWidget->setItem(i, 2, meanErrorItem);
        QTableWidgetItem* maxErrorItem = new QTableWidgetItem(QString::number(_estimators[i].calcMaxError()));
        ui->tableWidget->setItem(i, 3, maxErrorItem);
        double elapsedTime = timer.elapsed() / 1000.0;
        QTableWidgetItem* elapsedTimeItem = new QTableWidgetItem(QString::number(elapsedTime));
        ui->tableWidget->setItem(i, 4, elapsedTimeItem);
        _estimators[i].saveParams();
        _estimators[i].saveErrors();
        _estimators[i].saveRose();
        _estimators[i].saveElapsedTime(elapsedTime);
    }
    qDebug() << "READY";
}
