#ifndef UTILS_H
#define UTILS_H

#include <string>

#define MU_1 0
#define SIGMA_1 1
#define MU_2 2
#define SIGMA_2 3
#define MU_3 4
#define SIGMA_3 5
#define MU_4 6
#define SIGMA_4 7
#define ERROR 8

/**
 * Load data from the file to the global variable.
 */
void loadTargets(const std::string& path, double targets[72][19]);

/**
 * Calculate the Gauss function.
 */
double gauss(double x, double mu, double sigma);

/**
 * Solve the linear equation system.
 */
void solve(double a[4][4], double b[4], double* x);

/**
 * Calculate optimal alpha values.
 */
void calc_alphas(double* gene, double* target, double* alphas);

/**
 * Calculate the actual estimation at the measured points.
 */
void calc_actuals(double* gene, double* alphas, double* actuals);

/**
 * Calculate the sum of the square errors.
 */
double calc_sse(double* actual, double* target, int n);

/**
 * Generate random value on the [a, b) interval (a = 0, b = 1 by default).
 */
double unirand(double a = 0.0, double b = 1.0);

/**
 * Generate random integer value on the [a, b] interval.
 */
int randint(int a, int b);

/**
 * Choose an index from [0, k) interval.
 */
int choose(int k);

#endif // UTILS_H
