import csv
import os


SAMPLE_PATH = '../../samples'


def load_curves(path):
    """
    Load the data from the given path.
    :param path: path of the CSV file
    :return: value matrix (list of lists)
    """
    samples = [[] for _ in range(72)]
    with open(path) as sample_file:
        csv_reader = csv.reader(sample_file, delimiter=' ')
        for row in csv_reader:
            for j in range(72):
                samples[j].append(float(row[j]))
    return samples


def count_maximas(curve):
    """
    Count the maximum values of the curve.
    :param curve: list of floating point values
    :return: count of local maximas
    """
    assert len(curve) >= 3
    n = 0
    if curve[0] > curve[1]:
        n += 1
    for i in range(1, len(curve) - 1):
        if curve[i] > curve[i - 1] and curve[i] > curve[i + 1]:
            n += 1
    if curve[-1] > curve[-2]:
        n += 1
    assert n in [2, 3, 4]
    return n


if __name__ == '__main__':
    for file_name in os.listdir(SAMPLE_PATH):
        path = os.path.join(SAMPLE_PATH, file_name)
        curves = load_curves(path)
        counts = [count_maximas(curve) for curve in curves]
        print(file_name)
        print(counts)

