#!/bin/bash

for fn in $(find . | grep params.csv);
do
    name=$(echo $fn | cut -d"/" -f 3)
    echo $name
    python3 print_rose_values.py $fn > ${name}_rose.txt
done
