import csv
import math
import sys

CORRECTION = math.sqrt(2 * math.pi)


def read_params(path):
    with open(path) as param_file:
        field_names = ['index',
                       'alpha_1', 'mu_1', 'sigma_1',
                       'alpha_2', 'mu_2', 'sigma_2',
                       'alpha_3', 'mu_3', 'sigma_3',
                       'alpha_4', 'mu_4', 'sigma_4'
                      ]
        reader = csv.DictReader(param_file,
                                fieldnames=field_names,
                               delimiter=' ')
        params = []
        for row in reader:
            row.pop(None, None)
            item = {k: float(v) for k, v in row.items()}
            params.append(item)
        return params


def calc_rose_values(params):
    values = []
    for row in params:
        value = 0.0
        for i in ['1', '2', '3', '4']:
            value += row['mu_{}'.format(i)] * row['alpha_{}'.format(i)]
        value *= CORRECTION
        values.append(value)
    return values


path = sys.argv[1]
params = read_params(path)
phis = [5 * i for i in range(72)]
values = calc_rose_values(params)
print(values)
