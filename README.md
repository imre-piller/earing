# Earing

* Function approximation with the sum of gaussians

## Installation (on Windows)

It is necessary to install the Python environment. The official release can be downloaded from here:

* https://www.python.org/downloads/

(The application should work with the latest stable version.)

After, you should install a Python virtual environment. Navigate to the directory of this file and use the following command:
```
python -m venv .venv
```

For activating the Python virtual environment use the following:
```
.venv\Scripts\activate.bat
```

For installing the requirements:
```
(.venv) pip install -r requirements.txt
```

Some manual corrections are necessary in SciPy for proper curve fitting. Find the source file at:

* `.venv\lib\site-packages\scipy\optimize\_lsq\trf.py`

Near the line 234 add the following lines:

```python
v[dv != 0] *= scale_inv[dv != 0]
# ++++
for i in range(len(v)):
    if v[i] == 0:
        v[i] = 1e-8
# ++++
Delta = norm(x0 * scale_inv / v**0.5)
```

* The commented lines in the code snippet denotes only the place of the insertion.
* These lines are in the function `trf_bounds`.
* WARNING: It can be changed in the upcoming versions of SciPy.

For checking that the installation works properly, start the `earing-app.bat` file!

* It will display a command window and a small graphical application.
* For hiding the command window, create a desktop shortcut, and set *Minimized* in the drop down menu (of the *Properties* page).

After the successful installation, use can start the application by clicking to the shortcut icon.
