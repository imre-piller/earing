import os
import shutil

from matplotlib import pyplot as plt

from earing.curve import find_maximums
from earing.curve import find_inflexion_points
from earing.res_loader import load_res_data


def plot_estimated_centers(values, figure_path):
    """
    Plot the center estimations to the given figure.
    """
    chis = [i for i in range(0, 95, 5)]
    stem_height = 5

    fig, axs = plt.subplots(2, 1)

    axs[0].plot(chis, values)
    axs[0].set_xlim([0, 90])
    axs[0].set_ylim([-0.5, 5])
    axs[0].grid(True)

    maximums = find_maximums(values)
    for maximum in maximums:
        axs[0].stem([maximum * 5], [stem_height], 'r', use_line_collection=True)

    inflexion_points = find_inflexion_points(values)
    for inflexion_point in inflexion_points:
        axs[0].stem([inflexion_point * 5], [stem_height * 0.8], 'g', use_line_collection=True)

    axs[1].plot(chis, values)
    axs[1].set_xlim([0, 90])
    axs[1].set_ylim([-0.5, 5])
    axs[1].grid(True)

    fig.set_figheight(9)
    fig.tight_layout()

    plt.savefig(figure_path)
    plt.close()


if __name__ == '__main__':
    result_path = '/home/imre/center_estimation'
    if os.path.isdir(result_path):
        shutil.rmtree(result_path)
    os.mkdir(result_path)

    for dir_name in ['lagyitas_1', 'lagyitas_2', 'lagyitas_3']:
        os.mkdir(os.path.join(result_path, dir_name))
        for name in os.listdir(os.path.join('samples', dir_name)):
            if name.endswith('.json'):
                json_path = os.path.join('samples', dir_name, name)
                res_data = load_res_data(json_path)
                samples = res_data['220']
                os.mkdir(os.path.join(result_path, dir_name, name[:-5]))
                for i, values in enumerate(samples):
                    phi = i * 5
                    figure_name = f'phi_{phi:03d}.png'
                    figure_path = os.path.join(result_path, dir_name, name[:-5], figure_name)
                    plot_estimated_centers(values, figure_path)
