import json
import math
import os
import shutil
import sys
import time

import numpy as np
from scipy.optimize import curve_fit

from curve import *
from rose import *
from res_loader import load_res_data


ROSE_DIR_PATH = 'orig/rose_diagrams'


def check_output_dir(output_path):
    """
    Check that the output directory is exists and create when necessary.
    It also removes the directory content for clean start.
    """
    if os.path.isdir(output_path):
        shutil.rmtree(output_path)
    os.mkdir(output_path)


def fit_all_curves(sample_path, result_path, direction):
    """
    Find JSON files, create the directories and fit the curves.
    """
    check_output_dir(result_path)
    check_output_dir(ROSE_DIR_PATH)
    for volume in os.listdir(sample_path):
        volume_path = os.path.join(sample_path, volume)
        if os.path.isdir(volume_path):
            output_volume_path = os.path.join(result_path, volume)
            os.mkdir(output_volume_path)
            for fname in os.listdir(volume_path):
                if fname.endswith('.json'):
                    json_path = os.path.join(volume_path, fname)
                    png_dir_path = os.path.join(output_volume_path, fname[:-5])
                    os.mkdir(png_dir_path)
                    os.mkdir(os.path.join(png_dir_path, 'fitted_curves'))
                    process_sample(json_path, png_dir_path, direction)


def process_sample(json_path, png_dir_path, direction):
    """
    Process a sample in a json file and render the fitted curves.
    """
    print(f'{json_path} ---> {png_dir_path}')
    try:
        start_time = time.time()
        res_data = load_res_data(json_path)
        samples = res_data[direction]
        phi_values = [phi for phi in range(0, 95, 5)]
        phi_values = increase_resolution(phi_values)
        fitted_params = {}
        rose_values = []
        for i, values in enumerate(samples):
            phi = i * 5
            centers = find_centers(values)
            centers = join_closest_centers(centers)
            n_centers = len(centers)
            bounds = calc_halfway_bounds(centers)
            values = increase_resolution(values)
            for j in range(len(values)):
                if values[j] < 0:
                    values[j] = 0
            init_values = calc_init_values(centers)
            if n_centers < 7:
                plus_centers = calc_plus_centers(centers)
                for plus_center in plus_centers:
                    bounds[0] += (0, 0, 1)
                    bounds[1] += (1000, 90, 10)
                    init_values.extend([500, plus_center, 5])
            params, _ = curve_fit(sum_of_gaussians,
                                  np.array(phi_values),
                                  np.array(values),
                                  np.array(init_values),
                                  bounds=bounds,
                                  method='trf',
                                  maxfev=50000)
            gaussians = collect_gaussian_params(params)
            fitted_params[phi] = gaussians
            curve_path = os.path.join(png_dir_path, f'fitted_curves/phi_{phi:03d}.png')
            curve = {
                'title': f'phi = {phi}',
                'values': values,
                'centers': centers,
                'gaussians': gaussians,
                'path': curve_path
            }
            plot_curve(curve)
            rose_value = 0
            for gaussian in gaussians:
                rose_value += math.sin(math.radians(gaussian['mu'])) * gaussian['alpha']
            rose_values.append(rose_value)
        with open(os.path.join(png_dir_path, 'params.json'), 'w') as param_file:
            json.dump(fitted_params, param_file)
        save_rose_values(rose_values, os.path.join(png_dir_path, 'rose_values.txt'))
        rose_path = os.path.join(png_dir_path, 'rose.png')
        plot_rose_diagram(rose_values, rose_path)
        elapsed_time = time.time() - start_time
        with open(os.path.join(png_dir_path, 'elapsed_time.txt'), 'w') as time_file:
            time_file.write(str(elapsed_time))
    except Exception as e:
        with open(os.path.join(png_dir_path, 'error.txt'), 'w') as error_file:
            error_file.write(str(e))


if __name__ == '__main__':
    if len(sys.argv) == 4:
        fit_all_curves(sys.argv[1], sys.argv[2], sys.argv[3])
    elif len(sys.argv) == 1:
        print('usage: curve_fitter.py <sample path> <result path> <direction>')
    else:
        raise ValueError('Invalid number of command line arguments!')
