import os

import numpy as np
from scipy.optimize import curve_fit

from curve import *


for name in os.listdir('../samples'):
    path = os.path.join('../samples', name)
    print(f'Name:{name}')
    samples = load_samples(path)
    for i, values in enumerate(samples):
        centers = find_centers(values)
        if len(centers) < 3 or len(centers) > 5:
            print(f'-- centers {len(centers)}')
            bounds = [
                (0, centers[0] - 5, 1, 0, centers[1] - 5, 1),
                (1000, centers[0] + 5, 10, 1000, centers[1] + 5, 10)
            ]
            print(bounds)
            phi_values = [phi for phi in range(0, 95, 5)]
            params, pcov = curve_fit(sum_of_2_gaussians, np.array(phi_values), np.array(values), bounds=bounds)
            print(params)
            gaussians = [
                {'alpha': params[0], 'mu': params[1], 'sigma': params[2]},
                {'alpha': params[3], 'mu': params[4], 'sigma': params[5]}
            ]
            curve = {
                'title': f'n centers = {len(centers)}',
                'values': values,
                'centers': centers,
                'gaussians': gaussians,
                'path': f'/tmp/{name}_phi_{i * 5}.png'
            }
            plot_curve(curve)
        for i in range(len(centers) - 1):
            d = centers[i + 1] - centers[i]
            if d < 10:
                print(f'd = {d}')
