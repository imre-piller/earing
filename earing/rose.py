import json

import matplotlib.pyplot as plt


def plot_rose_diagram(rose_values, path):
    """
    Plot the rose diagram.
    :param rose_values: list of values
    :param path: path of the PNG file
    :return: None
    """
    plt.figure(figsize=(12, 10))
    ax = plt.axes()
    ax.grid()
    phis = [5 * i for i in range(72)]
    plt.plot(phis, rose_values)
    plt.savefig(path)
    plt.close()


def plot_rose_and_error_diagram(rose_values, error_values, n_curves, path):
    """
    Plot the rose diagram, error values and the number of curves for fitting.
    :param rose_values: list of rose diagram values
    :param error_values: errors by phi values
    :param n_cuves: number of the fitted curves
    :param path: path of the output PNG file
    :return: None
    """
    fig, axs = plt.subplots(3, 1)
    phis = [5 * i for i in range(72)]
    axs[0].plot(phis, error_values)
    axs[0].grid(True)
    axs[1].plot(phis, rose_values)
    axs[1].grid(True)
    axs[2].stem(phis, n_curves)
    fig.set_figheight(12)
    fig.tight_layout()
    plt.savefig(path)
    plt.close()
    

def save_rose_values(rose_values, path):
    """
    Save the rose values to a text file.
    :param rose_values: list of values
    :param path: path of the PNG file
    """
    with open(path, 'w') as value_file:
        for value in rose_values:
            value_file.write(f'{value}\n')


def print_rose_details(path):
    """
    Print the details of the rose diagram calculations.
    :param path: path of the parameter file
    :return: None
    """
    with open(path) as param_file:
        params = json.load(param_file)
        phis = [5 * i for i in range(72)]
        for phi in phis:
            value = 0
            for gauss in params[str(phi)]:
                print(f'    {gauss["mu"]} * {gauss["alpha"]} // {gauss["sigma"]}')
                value += gauss['mu'] * gauss['alpha']
            print(f'[phi = {phi}] => {value}')
            print()


if __name__ == '__main__':
    import numpy as np
    v = np.random.randn(72)
    plot_rose_and_error_diagram(v, v, v, '/tmp/ok.png')
    # print_rose_details('/tmp/params.json')
