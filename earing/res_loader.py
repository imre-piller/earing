import json


def load_res_data(json_path):
    """
    Load the res file data via the JSON file.
    """
    with open(json_path) as json_file:
        item = json.load(json_file)
        # WARN: The next line assumes that the name of the JSON and RES files are the same!
        res_path = json_path.replace('.json', '.RES')
        res_data = read_res_file(res_path, item['phi0'])
    return res_data


def read_res_file(res_path, phi0):
    """
    Read the data from the RES file.
    """
    res_data = {}
    with open(res_path) as res_file:
        lines = res_file.readlines()
        for direction in phi0:
            a, b = find_line_range(lines, direction)
            res_data[direction] = read_samples(lines[a:b], phi0[direction])
    return res_data


def find_line_range(lines, direction):
    """
    Find the [a, b) interval of the corresponding lines in the read RES data.
    """
    line_head = f' {direction}'
    print(f'"{line_head}"')
    for i, line in enumerate(lines):
        if line.startswith(line_head) and (lines[i + 1].startswith('  ')):
            a = i + 1
    b = a + 144
    return a, b


def read_samples(lines, phi0):
    """
    Read the samples in a matrix form.
    """
    values = collect_floating_values(lines)
    samples = reshape_samples_with_phi0(values, phi0)
    return samples


def collect_floating_values(lines):
    """
    Collect all floating point values from the list of strings.
    """
    values = []
    for line in lines:
        line = line.strip()
        splitted = line.split(' ')
        for s in splitted:
            if s != '':
                value = float(s)
                values.append(value)
    return values


def reshape_samples_with_phi0(values, phi0):
    """
    Reshape the data to a 72 x 19 matrix form.
    """
    assert len(values) == 72 * 18
    samples = [[phi0] for _ in range(72)]
    index = 0
    for _ in range(18):
        for i in range(72):
            samples[i].append(values[index])
            index += 1
    return samples
