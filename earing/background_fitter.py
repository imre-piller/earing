import json
import math
import os
import shutil
import sys
import time

import numpy as np
from scipy.optimize import curve_fit

from curve import *
from rose import *
from res_loader import read_res_file

SAMPLE_DIR_PATH = '../webapp/data/samples'
RESULT_DIR_PATH = '../webapp/data/results'


def collect_sample_ids() -> set[int]:
    """
    Collect the sample IDs by listing JSON files.
    """
    sample_ids = set()
    for file_name in os.listdir(SAMPLE_DIR_PATH):
        if file_name.endswith('.json'):
            sample_id = int(file_name.split('.')[0])
            sample_ids.add(sample_id)
    return sample_ids


def collect_result_ids() -> set[int]:
    """
    Collect the result IDs by listing directories.
    """
    result_ids = set()
    for dir_name in os.listdir(RESULT_DIR_PATH):
        if os.path.isdir(os.path.join(RESULT_DIR_PATH, dir_name)) and dir_name != 'tmp':
            result_id = int(dir_name)
            result_ids.add(result_id)
    return result_ids


def find_next_unprocessed_id() -> int:
    """
    Find the ID of the next unprocessed sample.
    """
    sample_ids = collect_sample_ids()
    result_ids = collect_result_ids()
    unprocessed_ids = sample_ids - result_ids
    if unprocessed_ids:
        return min(unprocessed_ids)
    else:
        return None


def load_fitting_data(sample_id) -> dict:
    """
    Load the parameters for curve fitting.
    """
    json_path = os.path.join(SAMPLE_DIR_PATH, f'{sample_id}.json')
    with open(json_path) as json_file:
        params = json.load(json_file)
    fitting_data = {
        'res_path': os.path.join(SAMPLE_DIR_PATH, f'{sample_id}.RES'),
        'direction': params['reflection'],
        'phi0': params['phi0'],
        'png_dir_path': os.path.join(RESULT_DIR_PATH, 'tmp')
    }
    return fitting_data


def process_sample(res_path, direction, phi0, png_dir_path):
    """
    Process a sample by reading the RES file.
    The resulted output will be placed into the given PNG directory.
    """
    print(f'{res_path} ---> {png_dir_path}')
    try:
        start_time = time.time()
        res_data = read_res_file(res_path, {direction: phi0})
        samples = res_data[direction]
        os.mkdir(png_dir_path)
        os.mkdir(os.path.join(png_dir_path, 'fitted_curves'))
        phi_values = [phi for phi in range(0, 95, 5)]
        phi_values = increase_resolution(phi_values)
        fitted_params = {}
        rose_values = []
        for i, values in enumerate(samples):
            phi = i * 5
            # print(f'{phi = }')
            centers = find_centers(values)
            centers = join_closest_centers(centers)
            n_centers = len(centers)
            bounds = calc_halfway_bounds(centers)
            values = increase_resolution(values)
            for j in range(len(values)):
                if values[j] < 0:
                    values[j] = 0
            init_values = calc_init_values(centers)
            if n_centers < 7:
                plus_centers = calc_plus_centers(centers)
                for plus_center in plus_centers:
                    bounds[0] += (0, 0, 1)
                    bounds[1] += (1000, 90, 10)
                    init_values.extend([500, plus_center, 5])
            params, _ = curve_fit(sum_of_gaussians,
                                  np.array(phi_values),
                                  np.array(values),
                                  np.array(init_values),
                                  bounds=bounds,
                                  method='trf',
                                  maxfev=50000)
            gaussians = collect_gaussian_params(params)
            fitted_params[phi] = gaussians
            curve_path = os.path.join(png_dir_path, f'fitted_curves/phi_{phi:03d}.png')
            curve = {
                'title': f'phi = {phi}',
                'values': values,
                'centers': centers,
                'gaussians': gaussians,
                'path': curve_path
            }
            plot_curve(curve)
            rose_value = 0
            for gaussian in gaussians:
                rose_value += math.sin(math.radians(gaussian['mu'])) * gaussian['alpha']
            rose_values.append(rose_value)
        with open(os.path.join(png_dir_path, 'params.json'), 'w') as param_file:
            json.dump(fitted_params, param_file)
        save_rose_values(rose_values, os.path.join(png_dir_path, 'rose_values.txt'))
        rose_path = os.path.join(png_dir_path, 'rose.png')
        plot_rose_diagram(rose_values, rose_path)
        elapsed_time = time.time() - start_time
        with open(os.path.join(png_dir_path, 'elapsed_time.txt'), 'w') as time_file:
            time_file.write(str(elapsed_time))
    except Exception as e:
        with open(os.path.join(png_dir_path, 'error.txt'), 'w') as error_file:
            error_file.write(str(e))


if __name__ == '__main__':
    while True:
        unprocessed_id = find_next_unprocessed_id()
        if unprocessed_id is not None:
            fitting_data = load_fitting_data(unprocessed_id)
            process_sample(**fitting_data)
            result_path = os.path.join(RESULT_DIR_PATH, str(unprocessed_id))
            shutil.move(fitting_data['png_dir_path'], result_path)
        else:
            time.sleep(5)
