import csv
import math
import os
import sys
import matplotlib.pyplot as plt
import numpy as np


def load_samples(path):
    """
    Load the intensity measurements from the given data file.
    :param path: path of the CSV file
    :return: value matrix (list of lists)
    """
    samples = [[] for _ in range(72)]
    with open(path) as sample_file:
        csv_reader = csv.reader(sample_file, delimiter=' ')
        for row in csv_reader:
            for j in range(72):
                samples[j].append(float(row[j]))
    return samples


def calc_gaussian(x, alpha, mu, sigma):
    """
    Calculate the Gauss function at the given point.
    :param x: value on the domain
    :param alpha: constant weight parameter
    :param mu: center of the curve
    :param sigma: standard deviance of the gauss function
    :return: function value at x
    """
    return (alpha / (math.sqrt(2 * math.pi) * sigma)) * np.exp(-((x - mu)*(x - mu))/(2 * sigma * sigma))


def calc_intensity_approximation(gaussians, chis):
    """
    Calculate the intensity approximation at certain points as the sum of gaussian functions.
    :param gaussians: list of gauss curve parameters in dictionaries
    :param chis: chi values
    :return: intensity curve approximation at the given point
    """
    intensities = []
    for chi in chis:
        intensity = 0
        for gaussian in gaussians:
            intensity += calc_gaussian(chi, **gaussian)
        intensities.append(intensity)
    return intensities


def find_maximums(values):
    """
    Find the local maximums of the values.
    :param values: list of floating point values
    :return: indices of the local maximum values
    """
    assert len(values) >= 4
    maximums = []
    if values[0] > values[1]:
        maximums.append(0)
    elif values[0] == values[1] and values[2] < values[1]:
        maximums.append(0)
    for i in range(len(values) - 2):
        if values[i + 1] > values[i] and values[i + 1] > values[i + 2]:
            maximums.append(i + 1)
    for i in range(len(values) - 3):
        if values[i + 1] == values[i + 2]:
            if values[i + 1] > values[i] and values[i + 2] > values[i + 3]:
                maximums.append(i + 1)
    if values[-1] > values[-2]:
        maximums.append(len(values) - 1)
    elif values[-1] == values[-2] and values[-3] > values[-2]:
        maximums.append(len(values) - 1)
    maximums.sort()
    return maximums


def find_sign_changes(values):
    """
    Find the indices where the sign has changed.
    :param values: iterable container of floating point values
    :return: indices of the sign changes
    """
    indices = []
    for i in range(len(values) - 1):
        if values[i] < 0 and values[i + 1] >= 0:
            indices.append(i)
        if values[i] >= 0 and values[i + 1] < 0:
            indices.append(i)
    return indices


def find_inflexion_points(values):
    """
    Find the inflexion points of the curve.
    :param values: iterable container of floating point values
    :return: indices of the inflexion points
    """
    diff_1 = np.diff(values)
    diff_2 = np.diff(diff_1)
    inflexion_points = [i + 2 for i in find_sign_changes(diff_2)]
    return inflexion_points


def find_shoulders(values):
    """
    Find the shoulders of the values
    :param values: list of floating point values
    :return: indices of the shoulder values
    """
    assert len(values) >= 4
    shoulders = []
    for i in range(len(values) - 3):
        m0 = values[i + 1] - values[i]
        m1 = values[i + 2] - values[i + 1]
        m2 = values[i + 3] - values[i + 2]
        if m0 > 0 and m1 >= 0 and m2 > 0:
            if m1 < m0 and m1 < m2:
                shoulders.append(i + 1)
        if m0 < 0 and m1 <= 0 and m2 < 0:
            if m1 > m0 and m1 > m2:
                shoulders.append(i + 2)
    return shoulders


def find_centers(intensities):
    """
    Find the centers of the approximation curves.
    :param intensities: the measured intensity values
    :return: list of chi values as the center approximations
    """
    maximums = find_maximums(intensities)
    # assert len(maximums) <= 5
    inflexion_points = find_inflexion_points(intensities)
    indices = sorted(set(maximums + inflexion_points))
    if False and len(indices) > 5:
        indices = set(maximums[:])
        i = 0
        while len(indices) < 5:
            indices.add(inflexion_points[i])
            i += 1
        indices = sorted(indices)
    centers = [5 * index for index in indices]
    return centers


def join_closest_centers(centers):
    """
    Join centers which distance is less or equal than 5.
    """
    joined_centers = centers[:]
    i = 0
    while i < len(joined_centers) - 1:
        if joined_centers[i + 1] - joined_centers[i] <= 5:
            new_center = (joined_centers[i] + joined_centers[i + 1]) / 2
            del joined_centers[i]
            del joined_centers[i]
            joined_centers.insert(i, new_center)
        else:
            i += 1
    return joined_centers


def sum_of_gaussians(x, *params):
    """
    Calculate the sum of the gaussian functions at the given point.
    """
    assert len(params) % 3 == 0
    s = 0
    for i in range(0, len(params), 3):
        alpha = params[i]
        mu    = params[i + 1]
        sigma = params[i + 2]
        s += calc_gaussian(x, alpha, mu, sigma)
    return s


def calc_init_values(centers):
    """
    Calculate the initial values from the centers.
    :param centers: estimated centers of the gauss curves
    :return: list of initial values
    """
    init_values = []
    for center in centers:
        init_values.extend([500, center, 5])
    return init_values


def calc_plus_centers(centers):
    """
    Calculate the centers of the additional gaussian curves.
    """
    values = [0] + centers + [90]
    max_distance = 0
    plus_centers = []
    for i in range(len(values) - 1):
        distance = values[i + 1] - values[i]
        if distance >= max_distance:
            plus_center = (values[i] + values[i + 1]) / 2
            if distance > max_distance:
                plus_centers = [plus_center]
                max_distance = distance
            else:
                plus_centers.append(plus_center)
    return plus_centers


def calc_bounds(centers):
    """
    Calculate the bounds of the parameters.
    :param centers: estimated centers of the gauss curves
    :return: List of 2-tuples with lower and upper bounds
    """
    assert len(centers) > 0
    lower_bounds = ()
    upper_bounds = ()
    for center in centers:
        lower_bound = center - 5
        if lower_bound < 0:
            lower_bound = 0
        upper_bound = center + 5
        if upper_bound > 90:
            upper_bound = 90
        lower_bounds += (0, lower_bound, 1)
        upper_bounds += (1000, upper_bound, 10)
    return [lower_bounds, upper_bounds]


def calc_halfway_bounds(centers):
    """
    Calculate the bounds of the parameters.
    :param centers: estimated centers of the gauss curves
    :return: List of 2-tuples with lower and upper bounds
    """
    assert len(centers) > 0
    halfways = []
    for i in range(len(centers) - 1):
        halfway = (centers[i] + centers[i + 1]) / 2
        halfways.append(halfway)
    halfways = [0] + halfways + [90]
    lower_bounds = ()
    upper_bounds = ()
    for i in range(len(halfways) - 1):
        lower_bounds += (0, halfways[i], 1)
        upper_bounds += (1000, halfways[i + 1], 10)
    return [lower_bounds, upper_bounds]


def extend_bounds(bounds):
    """
    Extend the bounds for 5 curves.
    :param bounds: List of 2-tuples with lower and upper bounds
    :return: List of 2-tuples with lower and upper bounds
    """
    assert len(bounds[0]) == len(bounds[1])
    extended_bounds = bounds
    while len(bounds[0]) != 15:
        bounds[0] += (0, 0, 1)
        bounds[1] += (1000, 90, 10)
    return extended_bounds


def collect_gaussian_params(params):
    """
    Collect gaussian curve parameters to a dictionary.
    :param params: list of estimated parameters
    :return: dictionary of gaussian parameters
    """
    assert len(params) % 3 == 0
    gaussians = []
    for i in range(0, len(params), 3):
        gaussians.append({
            'alpha': params[i], 'mu': params[i + 1], 'sigma': params[i + 2]
        })
    return gaussians


def plot_curve(curve):
    """
    Plot the curve value and the associated data.
    :param curve: curve data in a dictionary
    :return: None
    """
    assert 'title' in curve
    plt.xlabel('chi')
    plt.ylabel('value')
    plt.title(curve['title'])
    axes = plt.gca()
    axes.set_xlim([0, 90])
    axes.set_ylim([-0.2, 5])
    plt.grid(True)
    if 'centers' in curve:
        for i, center in enumerate(curve['centers']):
            if center == 0:
                curve['centers'][i] = 2
            if center == 90:
                curve['centers'][i] = 88
        # plt.stem(curve['centers'], [6] * len(curve['centers']), 'k', use_line_collection=True)
        plt.stem(curve['centers'], [6] * len(curve['centers']), 'k')
    if 'values' in curve:
        chi_values = increase_resolution([i for i in range(0, 95, 5)])
        plt.plot(chi_values, curve['values'], 'b')
    if 'gaussians' in curve:
        chi_values = [chi for chi in range(0, 91)]
        fitted_values = np.array([0] * 91, dtype=float)
        for gaussian in curve['gaussians']:
            gauss_values = [calc_gaussian(chi, **gaussian) for chi in chi_values]
            fitted_values += np.array(gauss_values)
            plt.plot(chi_values, gauss_values, 'g')
        plt.plot(chi_values, fitted_values, 'r')
    if 'path' in curve:
        plt.savefig(curve['path'])
        plt.close()
    else:
        plt.show()


def increase_resolution(values):
    """
    Increase the resolution.
    """
    new_size = len(values) * 2 - 1
    new_values = [0] * new_size
    for i in range(len(values) - 1):
        new_values[i * 2] = values[i]
        new_values[i * 2 + 1] = (values[i] + values[i + 1]) / 2
    new_values[-1] = values[-1]
    return new_values


def calc_errors(curve):
    """
    Calculate the MSE of the fitted curve at the measured points.
    :param curve: curve data in a dictionary
    :return: mean square error of the fitted curve
    """
    n_measurements = len(curve['values'])
    chi_values = [i * 5 for i in range(n_measurements)]
    fitted_values = np.array([0] * n_measurements, dtype=float)
    for gaussian in curve['gaussians']:
        gauss_values = [calc_gaussian(chi, **gaussian) for chi in chi_values]
        fitted_values += np.array(gauss_values)
    error_value = (np.square(np.array(curve['values']) - fitted_values)).mean(axis=0)
    error_value = float(error_value)
    return error_value
