import math
import os
import time
from threading import Thread

import tkinter as tk
from tkinter.filedialog import askopenfile
from tkinter.filedialog import askdirectory

import numpy as np
from scipy.optimize import curve_fit

from curve import *
from rose import *
from res_loader import read_res_file

res_file_path = None
miller_index = None
output_dir_path = None

PADDING = 5

def select_res_file():
    global res_file_path
    res_file = askopenfile(mode='r', filetypes=[('RES', '*.RES')])
    if res_file is None:
        return
    res_file_path = res_file.name
    res_path_label.config(text=res_file_path)
    miller_values = read_miller_values(res_file)
    create_miller_frame(miller_values)
    res_file.close()

def read_miller_values(res_file):
    miller_values = []
    for _ in range(3):
        _ = next(res_file)
    for _ in range(3):
        row = next(res_file)
        miller_value = row.strip().split(' ')[0]
        miller_values.append(miller_value)
    return miller_values

def create_miller_frame(miller_values):
    global input_frame
    global miller_index
    miller_frame = tk.Frame(input_frame, relief=tk.GROOVE, borderwidth=3)
    miller_label = tk.Label(input_frame, text='Miller index:')
    miller_label.grid(row=1, column=0, sticky='w', padx=PADDING, pady=PADDING)
    miller_index = tk.StringVar(value=miller_values[0])
    for miller_value in miller_values:
        miller_rb = tk.Radiobutton(miller_frame, text=miller_value, variable=miller_index, value=miller_value)
        miller_rb.pack()
    miller_frame.grid(row=1, column=1, sticky='w', padx=PADDING, pady=PADDING)

def select_output_dir():
    global output_dir_path
    path = askdirectory()
    output_dir_path = path
    output_dir_label.config(text=output_dir_path)

def check_inputs():
    global chi0_entry
    if res_file_path is None:
        raise ValueError('The RES file has not selected yet!')
    chi0_text = chi0_entry.get()
    if chi0_text.strip() == '':
        raise ValueError('The Chi0 value has not set yet!')
    try:
        _ = float(chi0_text)
    except:
        raise ValueError(f'The "{chi0_text}" value for Chi0 is invalid!')
    if output_dir_path is None or output_dir_path == '':
        raise ValueError('The output directory has not selected yet!')
    if list(os.listdir(output_dir_path)) != []:
        raise ValueError('The selected output directory is not empty!')

def evaluate_sample():
    global error_label
    global miller_index
    global evaluate_button
    try:
        check_inputs()
        error_label.config(text='')
        evaluate_button['state'] = 'disabled'
        evaluate_button.config(text='Evaluation is in progress ...')
        fitting_args = (
            res_file_path,
            miller_index.get(),
            float(chi0_entry.get().strip()),
            output_dir_path
        )
        thread = Thread(target=evaluation_process, args=fitting_args)
        thread.start()
    except ValueError as error:
        error_label.config(text=f'ERROR: {error}')

def evaluation_process(res_path, direction, phi0, png_dir_path):
    global evaluate_button
    process_sample(res_path, direction, phi0, png_dir_path)
    evaluate_button['background'] = '#AAFFAA'
    evaluate_button.config(text='Ready!')

def process_sample(res_path, direction, phi0, png_dir_path):
    """
    Process a sample by reading the RES file.
    The resulted output will be placed into the given PNG directory.
    """
    # print(f'{res_path} ---> {png_dir_path}')
    try:
        start_time = time.time()
        res_data = read_res_file(res_path, {direction: phi0})
        samples = res_data[direction]
        # os.mkdir(png_dir_path)
        os.mkdir(os.path.join(png_dir_path, 'fitted_curves'))
        phi_values = [phi for phi in range(0, 95, 5)]
        phi_values = increase_resolution(phi_values)
        fitted_params = {}
        rose_values = []
        for i, values in enumerate(samples):
            phi = i * 5
            # print(f'{phi = }')
            centers = find_centers(values)
            centers = join_closest_centers(centers)
            n_centers = len(centers)
            bounds = calc_halfway_bounds(centers)
            values = increase_resolution(values)
            for j in range(len(values)):
                if values[j] < 0:
                    values[j] = 0
            init_values = calc_init_values(centers)
            if n_centers < 7:
                plus_centers = calc_plus_centers(centers)
                for plus_center in plus_centers:
                    bounds[0] += (0, 0, 1)
                    bounds[1] += (1000, 90, 10)
                    init_values.extend([500, plus_center, 5])
            params, _ = curve_fit(sum_of_gaussians,
                                  np.array(phi_values),
                                  np.array(values),
                                  np.array(init_values),
                                  bounds=bounds,
                                  method='trf',
                                  maxfev=50000)
            gaussians = collect_gaussian_params(params)
            fitted_params[phi] = gaussians
            curve_path = os.path.join(png_dir_path, f'fitted_curves/phi_{phi:03d}.png')
            curve = {
                'title': f'phi = {phi}',
                'values': values,
                'centers': centers,
                'gaussians': gaussians,
                'path': curve_path
            }
            plot_curve(curve)
            rose_value = 0
            for gaussian in gaussians:
                rose_value += math.sin(math.radians(gaussian['mu'])) * gaussian['alpha']
            rose_values.append(rose_value)
        with open(os.path.join(png_dir_path, 'params.json'), 'w') as param_file:
            json.dump(fitted_params, param_file)
        save_rose_values(rose_values, os.path.join(png_dir_path, 'rose_values.txt'))
        rose_path = os.path.join(png_dir_path, 'rose.png')
        plot_rose_diagram(rose_values, rose_path)
        elapsed_time = time.time() - start_time
        with open(os.path.join(png_dir_path, 'elapsed_time.txt'), 'w') as time_file:
            time_file.write(str(elapsed_time))
    except Exception as e:
        with open(os.path.join(png_dir_path, 'error.txt'), 'w') as error_file:
            error_file.write(str(e))

window = tk.Tk()
window.title('Earing - Gaussian Curve Fitting')
# window.geometry('720x360')

input_frame = tk.Frame(window, relief=tk.SOLID, borderwidth=1)
input_frame.grid(row=0, column=0, padx=PADDING, pady=PADDING)

res_upload_button = tk.Button(input_frame, text='Select RES file', command=select_res_file)
res_upload_button.grid(row=0, column=0, sticky='we', padx=PADDING, pady=PADDING)
res_path_label = tk.Label(input_frame, text='')
res_path_label.grid(row=0, column=1, sticky='w', padx=PADDING, pady=PADDING)

# res_upload_button['background'] = 'orange'

chi0_label = tk.Label(input_frame, text='Chi0:')
chi0_label.grid(row=2, column=0, sticky='w', padx=PADDING, pady=PADDING)
chi0_entry = tk.Entry(input_frame, width=80)
chi0_entry.grid(row=2, column=1, sticky='w', padx=PADDING, pady=PADDING)

output_dir_button = tk.Button(input_frame,
                              text='Select output directory',
                              command=select_output_dir)

output_dir_button.grid(row=3, column=0, sticky='we', padx=PADDING, pady=PADDING)

output_dir_label = tk.Label(input_frame, text='')
output_dir_label.grid(row=3, column=1, sticky='w', padx=PADDING, pady=PADDING)

evaluate_button = tk.Button(window, text='Evaluate sample', command=evaluate_sample)
evaluate_button.grid(row=1, column=0, padx=PADDING, pady=PADDING)

error_label = tk.Label(window, text='', fg='#F00')
error_label.grid(row=4, column=0, sticky='w', padx=PADDING, pady=PADDING)

window.mainloop()
