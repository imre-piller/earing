# SciPy patch

## Error message

```
.venv\lib\site-packages\scipy\optimize\_lsq\trf.py:234:
RuntimeWarning: divide by zero encountered in divide
Delta = norm(x0 * scale_inv / v**0.5)
```

## Patch

The following lines should be inserted:
```
v[dv != 0] *= scale_inv[dv != 0]
# ++++
for i in range(len(v)):
    if v[i] == 0:
        v[i] = 1e-8
# ++++
Delta = norm(x0 * scale_inv / v**0.5)
```
