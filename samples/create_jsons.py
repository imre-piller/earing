import json
import os
import sys

if __name__ == '__main__':
    if len(sys.argv) == 3:
        name_file_path = sys.argv[1]
        json_dir_path = sys.argv[2]
        if os.path.isdir(json_dir_path):
            with open(name_file_path) as fname_file:
                for row in fname_file:
                    item = {}
                    s = row.strip().split(' ')
                    name = s[0]
                    item['res_file'] = name + '.RES'
                    item['phi0'] = {
                        '110': float(s[1]),
                        '200': float(s[2]),
                        '211': float(s[3])
                    }
                    item['weights'] = []
                    with open(os.path.join(json_dir_path, f'{name}.json'), 'w') as json_file:
                        json.dump(item, json_file)
        else:
            print('ERROR: The directory for JSON files does not exists!')
    else:
        print('ERROR: Invalid number of command line arguments!')
        print('Usage: create_jsons.py <name file path> <json dir path>')
