import json
import logging
import os

from typing import Annotated
from pydantic import BaseModel
from fastapi import FastAPI, File, UploadFile


SAMPLE_DIR_PATH = './data/samples'
RESULT_DIR_PATH = './data/results'


class Sample(BaseModel):
    resFileId: int
    reflection: str
    phi0: float
    description: str


class SampleRequest(BaseModel):
    sample: Sample


app = FastAPI()
logger = logging.getLogger(__name__)


def calc_new_id(path: str, extension: str) -> int:
    """
    Calculate a new, unique identifier from the content of a directory.
    """
    file_names = os.listdir(path)
    new_id = 1
    for name in file_names:
        if name.endswith(extension):
            parts = name.split('.')
            n = int(parts[0])
            if n >= new_id:
                new_id = n + 1
    return new_id


def calc_sample_id() -> int:
    """
    Calculate the unique identifier of the sample.
    """
    return calc_new_id(SAMPLE_DIR_PATH, '.json')


def calc_res_file_id() -> int:
    """
    Calculate the unique identifier of the sample.
    """
    return calc_new_id(SAMPLE_DIR_PATH, '.RES')


def save_sample(sample) -> int:
    """
    Save the sample data to a JSON file.
    """
    sample_id = calc_sample_id()
    file_name = f'{sample_id}.json'
    with open(os.path.join(SAMPLE_DIR_PATH, file_name), 'w') as sample_file:
        json.dump(sample, sample_file)
    return sample_id


@app.post('/api/uploadResFile')
async def upload_res_file(resFile: UploadFile):
    res_file_id = calc_res_file_id()
    res_file_name = f'{res_file_id}.RES'
    file_path = os.path.join(SAMPLE_DIR_PATH, res_file_name)
    with open(file_path, 'wb') as res_file:
        res_file.write(await resFile.read())
    return {'resFileId': res_file_id}


@app.post('/api/createSample')
def create_sample(sample_request: SampleRequest):
    sample = sample_request.sample
    sample_id = save_sample(sample.dict())
    return {'sampleId': sample_id}


def get_sample_status(sample_id) -> str:
    """
    Get the status of the given sample.
    """
    result_path = os.path.join(RESULT_DIR_PATH, str(sample_id))
    if os.path.isdir(result_path):
        return 'Ready'
    else:
        return 'Evaluation is in progress'


@app.get('/api/collectSamples')
def collect_samples():
    samples = []
    for file_name in os.listdir(SAMPLE_DIR_PATH):
        if file_name.endswith('.json'):
            with open(os.path.join(SAMPLE_DIR_PATH, file_name)) as json_file:
                sample = json.load(json_file)
                sample['id'] = int(file_name.split('.')[0])
                sample['status'] = get_sample_status(sample['id'])
                samples.append(sample)
    samples.sort(key=lambda x: -x['id'])
    return {'samples': samples}
