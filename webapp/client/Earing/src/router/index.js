import { createRouter, createWebHistory } from 'vue-router'
import SampleList from '../components/SampleList.vue'
import NewSample from '../components/NewSample.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'sample-list',
      component: SampleList
    },
    {
      path: '/sample-list',
      name: 'sample-list',
      component: SampleList
    },
    {
      path: '/new-sample',
      name: 'new-sample',
      component: NewSample
    }
  ]
})

export default router
