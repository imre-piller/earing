import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';

import FileUpload from 'primevue/fileupload';

const app = createApp(App)

app.use(router)
app.use(PrimeVue)

app.component('FileUpload', FileUpload);

app.mount('#app')
