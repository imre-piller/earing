import os

csv_names = set()
res_names = set()
for name in os.listdir('samples'):
    subdir_path = os.path.join('samples', name)
    for file_name in os.listdir(subdir_path):
        if file_name.endswith('csv'):
            csv_names.add(file_name[:-4])
        elif file_name.endswith('RES'):
            res_names.add(file_name[:-4])
        else:
            raise ValueError(f'Invalid file! {file_name}')
print('== Missing RES files')
missing = sorted(list(csv_names - res_names))
for name in missing:
    print(name)
print('')
print('== Additional RES files')
print(res_names - csv_names)

