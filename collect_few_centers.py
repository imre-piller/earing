#
# Collect and plot the samples where the centers are fewer than needed.
#

import os

from earing.curve import find_maximums
from earing.curve import find_inflexion_points
from earing.res_loader import load_res_data

from center_estimation import plot_estimated_centers

output_dir = '/tmp/fewer'

for dir_name in ['lagyitas_1', 'lagyitas_2', 'lagyitas_3']:
    for name in os.listdir(os.path.join('samples', dir_name)):
        if name.endswith('.json'):
            print(f'{name} ...')
            json_path = os.path.join('samples', dir_name, name)
            res_data = load_res_data(json_path)
            samples = res_data['220']
            for i, values in enumerate(samples):
                phi = i * 5
                maximums = find_maximums(values)
                inflexion_points = find_inflexion_points(values)
                n_maximums = len(maximums)
                n_inflexions = len(inflexion_points)
                n_centers = n_maximums + n_inflexions
                if n_centers < 5:
                    figure_name = f'{name[:-5]}_phi_{phi:03d}.png'
                    figure_path = os.path.join(output_dir, figure_name)
                    plot_estimated_centers(values, figure_path)
